<ul class="o-list-unstyled c-header__nav-list">
[*>NODE]
</ul>
[>NODE]
    <li class="c-header__nav-item">
    [?ENABLED]
        <a class="c-header__nav-link" href="[=URL]">[=TEXT] [?NODE][/?]</a>
    [?ELSE]
        <a class="c-header__nav-link" href="#">[=TEXT] [?NODE][/?]</a>
    [/?]
    </li>
[/>]
