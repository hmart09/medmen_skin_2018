﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="MedmenCorporate_2018.Controls.Footer" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<footer class="c-footer" role="contentinfo">
    <section class="c-footer__top-section">
    <div class="o-container o-container--padded">
        <div class="c-footer__layout">
        <nav class="c-footer-nav">
            <ul class="o-list-inline c-footer-nav__list">
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/stores">Store Locator</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/what-we-do">Facilities</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/who-we-are">Our Team</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/newsroom">Newsroom</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/contact">Contact Us</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/stores">Retail</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="/who-we-are">Our Story</a></li>
            <li class="c-footer-nav__item"><a class="c-footer-nav__link" href="https://medmen.catsone.com/careers/index.php?m=portal&amp;a=listings&amp;portalID=66363">Careers</a></li>
            <li class="c-footer-nav__item c-footer-nav__item--full">
                <ul class="o-list-inline c-social u-spacing-none">
                <li class="c-social__item"><a class="c-social__item-link" href="https://www.facebook.com/ShopMedMen" target="_blank"><span class="u-sr-only">item.title</span>
                    <svg class="c-social__item-icon" width="20" height="20" viewbox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z"></path>
                    </svg></a></li>
                <li class="c-social__item"><a class="c-social__item-link" href="https://twitter.com/ShopMedMen" target="_blank"><span class="u-sr-only">item.title</span>
                    <svg class="c-social__item-icon" width="20" height="20" viewbox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1684 408q-67 98-162 167 1 14 1 42 0 130-38 259.5t-115.5 248.5-184.5 210.5-258 146-323 54.5q-271 0-496-145 35 4 78 4 225 0 401-138-105-2-188-64.5t-114-159.5q33 5 61 5 43 0 85-11-112-23-185.5-111.5t-73.5-205.5v-4q68 38 146 41-66-44-105-115t-39-154q0-88 44-163 121 149 294.5 238.5t371.5 99.5q-8-38-8-74 0-134 94.5-228.5t228.5-94.5q140 0 236 102 109-21 205-78-37 115-142 178 93-10 186-50z"></path>
                    </svg></a></li>
                <li class="c-social__item"><a class="c-social__item-link" href="https://instagram.com/ShopMedMen" target="_blank"><span class="u-sr-only">item.title</span>
                    <svg class="c-social__item-icon" width="20" height="20" viewbox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm138 0q0 164-115 279t-279 115-279-115-115-279 115-279 279-115 279 115 115 279zm108-410q0 38-27 65t-65 27-65-27-27-65 27-65 65-27 65 27 27 65zm-502-220q-7 0-76.5-.5t-105.5 0-96.5 3-103 10-71.5 18.5q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103-3 96.5 0 105.5.5 76.5-.5 76.5 0 105.5 3 96.5 10 103 18.5 71.5q20 50 58 88t88 58q29 11 71.5 18.5t103 10 96.5 3 105.5 0 76.5-.5 76.5.5 105.5 0 96.5-3 103-10 71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103 3-96.5 0-105.5-.5-76.5.5-76.5 0-105.5-3-96.5-10-103-18.5-71.5q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10-96.5-3-105.5 0-76.5.5zm768 630q0 229-5 317-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124t-124-322q-5-88-5-317t5-317q10-208 124-322t322-124q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z"></path>
                    </svg></a></li>
                </ul>
            </li>
            </ul>
        </nav><a class="u-spacing-20" href="/">
            <svg width="125" height="36" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 243.9 73.3">
            <path fill="#94070a" d="M206.7 16.9h12.2v6.6c2.2-4.2 5-7.6 9.8-7.6 7 0 10.6 6 10.6 14.5v41.9h-12.2V33.1c0-4-1.5-6.3-4-6.3s-4.2 2.3-4.2 6.3v39.1h-12.2V16.9zM85.3 47.1v-5c0-19.7 5.2-26.2 11.8-26.2 5.4 0 7.9 4.4 9.7 7.7V0H119v72.3h-12.2v-6.9c-1.8 3.3-4.3 8-9.7 8-6.7-.1-11.8-6.4-11.8-26.3m21.6-.6v-3.7c0-9.8-1.8-15.2-4.9-15.2-3.1 0-4.5 5.4-4.5 15.2v3.7c0 9.9 1.4 15.2 4.5 15.2s4.9-5.3 4.9-15.2M0 0h12.5l8.9 26.4L30.3 0h12.4v72.3h-12V28.6L21.5 56h-.2l-9.2-27.4v43.7H0zM59 48.6c0 4.8.7 13.8 5.5 13.8 2.9 0 3.4-4.1 4.2-8.6h12.1C80 60.3 76 73.3 64.2 73.3c-12.7 0-18.5-13.6-18.5-28.7 0-15.4 6.1-28.5 18.3-28.5 12.8 0 17.2 14.6 17.2 28.8v3.7H59zm9.9-10.4c-.5-4.5-.4-11-5-11-3.8 0-4.7 7.3-4.9 11h9.9zM122.2 0h12.5l8.9 26.4L152.5 0h12.4v72.3h-12V28.6L143.7 56h-.2l-9.2-27.4v43.7h-12.1zM181.2 48.6c0 4.8.7 13.8 5.5 13.8 2.9 0 3.4-4.1 4.2-8.6H203c-.7 6.3-4.8 19.4-16.6 19.4-12.7 0-18.5-13.6-18.5-28.7 0-15.4 6.1-28.5 18.3-28.5 12.8 0 17.2 14.6 17.2 28.8v3.7h-22.2zm9.9-10.4c-.5-4.5-.4-11-5-11-3.8 0-4.7 7.3-4.9 11h9.9zM242.6 17.3c.3.1.5.3.7.5.2.2.4.4.5.7.1.3.2.6.2.9 0 .3-.1.6-.2.9-.1.3-.3.5-.5.7-.2.2-.4.4-.7.5-.3.1-.6.2-.9.2-.3 0-.6-.1-.9-.2-.3-.1-.5-.3-.7-.5s-.4-.4-.5-.7c-.1-.3-.2-.6-.2-.9 0-.3.1-.6.2-.9.1-.3.3-.5.5-.7.2-.2.4-.4.7-.5.3-.1.6-.2.9-.2.3 0 .6.1.9.2m-1.7.3c-.2.1-.4.2-.6.4-.2.2-.3.4-.4.6-.1.2-.1.5-.1.8 0 .3 0 .5.1.8.1.2.2.4.4.6.2.2.4.3.6.4.2.1.5.2.8.2.3 0 .5-.1.8-.2.2-.1.4-.2.6-.4.2-.2.3-.4.4-.6.1-.2.1-.5.1-.8 0-.3-.1-.5-.1-.8-.1-.2-.2-.4-.4-.6-.2-.2-.4-.3-.6-.4-.2-.1-.5-.2-.8-.2-.3 0-.6.1-.8.2m0 3.1v-2.6h1c.3 0 .5.1.7.2.1.1.2.3.2.5 0 .1 0 .2-.1.3 0 .1-.1.2-.1.2-.1.1-.1.1-.2.1s-.2.1-.3.1l.8 1.2h-.4l-.7-1.2h-.5v1.2h-.4zm1-1.5c.1 0 .2 0 .3-.1.1 0 .1-.1.2-.1 0-.1.1-.1.1-.3 0-.1 0-.2-.1-.2 0-.1-.1-.1-.1-.1-.1 0-.1-.1-.2-.1h-.8v.9h.4c0 .1.1.1.2 0"></path>
            </svg></a>
        </div>
    </div>
    </section>
    <section class="c-footer__bottom-section">
    <div class="o-container o-container--padded">
        <ul class="o-list-unstyled c-footer-links">
        <li class="c-footer-links__item c-footer-links__item--copyright">&copy; 2018 MME, LLC.</li>
        <li class="c-footer-links__item"><a class="c-footer-links__link" href="/privacy-policy">Privacy Policy</a></li>
        <li class="c-footer-links__item"><a class="c-footer-links__link" href="/terms-use">Terms of Use</a></li>
        <li class="c-footer-links__item"><a class="c-footer-links__link" href="/legal">Legal Disclaimers</a></li>
        </ul>
    </div>
    </section>
</footer>

<span class="c-privacy-policy__close"><a class="close-button" href="#">X</a></span>
        <div class="c-privacy-policy__modal--420" id="modal">
          <h3>Privacy Policy</h3>
          <p class="u-color-red u-font-demibold">Last Updated: March 18, 2018</p>
          <h4>Purpose of Policy</h4>
          <p>MM Enterprises, USA, LLC, a Delaware limited liability company, doing business as “MedMen” (“us,” “we,” or the “Company”) is committed to respecting the privacy rights associated with users of the Company’s Internet website located at http://www.MedMen.com (the “Site”). We created this Privacy Policy (this “Policy”) to give you confidence as you visit and use the Site, and to demonstrate our commitment to fair information practices and the protection of privacy. This Policy is only applicable to the Site, and not to any other websites that you may be able to access from the Site, each of which may have data collection and use practices and policies that differ materially from this Policy.</p>
          <h4>Agreement to Terms of Privacy Policy and Terms of Use</h4>
          <p>All activities in which you may engage on the Site are voluntary. You are not required to provide any personal information to us unless you choose to access features on the Site that require such information. If you do not agree with the terms of this Policy or other terms of use on this Site, then you should not provide us with personal information and immediately exit and discontinue your use of the Site. The terms of use of the Site are expressly incorporated herein by reference and made a part of this Policy. A complete statement of Company’s terms of use can be found by clicking here.   By using the Site, you signify that you agree to the terms of this Policy as well as to our terms of use.</p>
          <h4>Effective Date of and Changes to Privacy Policy.</h4>
          <p>This Policy is effective as of March 12, 2018, and will remain in effect except with respect to any of its provisions that are changed in the future, in which case the changes will become effective on the date they are posted on the Site. We reserve the right to change this Policy at any time and from time to time in our sole and absolute discretion without the duty to notify you. Changes, modifications, additions, or deletions will be effective immediately on their posting to the Site. You should check this Policy periodically as its terms may change from time to time without prior notice to you. Your continued use of the Site after we post any such modifications will constitute your acknowledgment of the modified policy and your agreement to abide and be bound by the modified policy. We will also revise the “last updated” date found at the beginning of this Policy when we post changes to it.</p>
          <h4>Disclaimers</h4>
          <p>This Policy does not extend to anything that is inherent in the operation of the Internet, and therefore beyond the Company’s control, and is not to be applied in any manner contrary to applicable law or governmental regulation. This Policy only applies to information collected through the Site. This Policy does not apply to any information we may collect from you in any place other than the Site.</p>
          <h4>California Use Only</h4>
          <p>The Site is controlled and operated by the Company from its offices in the State of California. The Company makes no representation that any of the materials or the services to which you have been given access are available or appropriate for use in other locations. Your use of or access to the Site should not be construed as Company’s purposefully availing itself of the benefits or privilege of doing business in any state or jurisdiction other than California.</p>
          <h4>No Use of the Site by Persons Under 21 Permitted</h4>
          <p>The Company intends only persons who are 21 years or older to use the Site and any use of the Site by persons under the age of 21 is strictly prohibited. Personal information submitted by a person under the age of 21 will not be accepted. Any general information provided by a person under the age of 21 and gathered (for example, through the use of cookies) during his or her visit may be used as indicated in this Policy.</p>
          <h4>Notice Concerning Children</h4>
          <p>We are a mature audience site, and do not direct any of our content specifically at children under 13 years of age. We understand and are committed to respecting the sensitive nature of children’s privacy online. If we learn or have reason to suspect that a Site user is under age 13, we will promptly delete any personal information in that user’s account.</p>
          <h4>Information Collection Practices</h4>
          <p><b>What Basic Information Does the Company Collect?</b> We may ask you for certain information referred to on the Site as “personal information,” which includes information that pertains to your identity. Such information may include, but is not limited to, items such as your name, address, date of birth, age, gender, state of residence, e-mail address, telephone number, and/or financial data (which may include your credit card or debit card number). You may be required to provide personal information to access or use certain parts of our website or features of the Site. If you do not provide the requested personal information, you may not be able to access or use the features of the Site where such information is requested.</p>
          <p><b>What Additional Information Does Company Collect?</b> Automatic Collection. Our servers automatically recognize visitors’ domain names and IP addresses (the number assigned to computers on the Internet). No personal information about you is revealed in this process. The Site may also gather anonymous “traffic data” that does not personally identify you, but that may be helpful for marketing purposes or for improving the services we offer.</p>
          <p><b>Cookies.</b> From time to time, we may use the standard “cookies” feature of major browser applications, which allows us to store a small piece of data on your computer about your visit to the Site. We do not set any personally identifiable information in cookies, nor do we employ any data capture mechanisms on the Site other than cookies. Cookies help us learn which areas of the Site are useful and which areas need improvement. You can choose whether to accept cookies by changing the settings on your browser. However, if you choose to disable this function, your experience at the Site may be diminished and some features may not work as they were intended.</p>
          <p>Click Stream Data. When you visit the Site we may also collect “click stream data” (server address, domain name). This information can be combined with information you have provided to us by registering, for example, which will enable us to analyze and better customize your visits. We may use click stream data for traffic analysis or e-commerce analysis of our website, to determine which features of our site are most or least effective or useful to you.</p>
          <p>
            Personal Information. We may use your personal information to verify your identity, to check your qualifications, and to follow up with transactions initiated on the Site. Additionally, we share certain categories of information we collect from you in the ways described in this Policy.
            We may share demographic data with advertisers and other third parties only on an aggregate (i.e., non-personally-identifiable) basis. We may share contact data and financial data with our business partners who assist us by performing core services (such as hosting or data storage and security) related to our operation of the Site. If you do not want us to share your contact data with any third parties, please contact us in accordance with the “Contact Us” section below and expressly notify us of such request. Additionally, we may analyze visits to the Site and learn about the interests of our visitors in the aggregate and also on a personal level to better understand your interests and needs, so we can improve our products and services and deliver to you the type of content, features and promotions that you are most interested in.
          </p>
          <p>Anonymous Information. We may use anonymous information to analyze the Site traffic, but we do not intend to examine this information for individually identifying information. In addition, we may use anonymous IP addresses to help diagnose problems with our server, to administer the Site, or to display the content according to your preferences. Traffic and transaction information may also be shared with business partners and advertisers on an aggregate and anonymous basis.</p>
          <p>Use of Cookies. We may use cookies to deliver content specific to your interests, to save your password, if applicable, so you don’t have to re-enter it each time you visit the Site, or for other purposes. Promotions or advertisements displayed on the Site may contain cookies. We do not have access to or control over information collected by outside advertisers on the Site.</p>
          <p>Disclosure of Personal Information. We may disclose personal information if required to do so by law or in the good-faith belief that such action is necessary to (1) conform to the edicts of the law or comply with legal process served on the Company or its affiliates, (2) protect and defend the rights or property of the Company or the users of the Site, or (3) act under exigent circumstances to protect the safety of the public or users of the Site.</p>
          <p>Sale of Information. In order to accommodate changes in our business, we may sell portions of our Company or purchase other companies or assets, including the information collected through the Site. If Company or substantially all of its assets are acquired, customer information will be one of the assets transferred to the acquirer.</p>
          <p>Access to Information. At this time, we do not maintain any procedures for you to review or request changes to the information that we collect about you, except that you may request that we remove all information about you from our database by contacting us in accordance with the “Contact Us” section below and expressly requesting such removal.</p>
          <p>We understand and respect that not all individuals may want to allow us to share their information with other select companies. If you do not want us to share your information, please contact us in accordance with the “Contact Us” section below. Upon receipt of your express written request not to share your information we will remove your name from lists we share with other companies as soon as reasonably practicable. When contacting us, please clearly state your request and your name, mailing address, email address, and phone number.</p>
          <p>Notwithstanding your request, under the following circumstances, we may still be required to share your personal information:<br>If we respond to subpoenas, court orders or legal process, or if we need to establish or exercise our legal rights or defend against legal claims.
                  If we believe it is necessary to share such information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our terms of use or as otherwise required by law.
                  If we believe it is necessary to restrict or inhibit any user from using the Site, including, without limitation, by means of "hacking" or defacing any portion thereof.
          </p>
          <p>California's "Do Not Track" Notice. "Do Not Track" ("DNT") is a preference you can set in your web browser to let the sites you visit know that you do not want them collecting information about you. The Site does not currently respond to "Do Not Track" settings. For further details regarding DNT, visit donottrack.us.</p>
          <h4>Security</h4>
          <p>The Site has security measures in place to prevent the loss, misuse, and alteration of the information that we obtain from you. Please keep in mind, however, that whenever you give out personal information online there is a risk that third parties may intercept and use that information.<br>      While the Company strives to protect your personal information and privacy, we cannot guarantee the security of any information you disclose online. By using the Site, you expressly acknowledge and agree that we do not guarantee the security of any data provided to or received by us through the Site and that any personal information, general information, or other data or information received from you through the Site is provided to us at your own risk, which you expressly assume.</p>
          <h4>Third Party Websites</h4>
          <p>The Site may contain links to other websites. If you choose to visit other websites, we are not responsible for the privacy practices or content of those other websites, and it is your responsibility to review the privacy policies at those websites to confirm that you understand and agree with their policies.</p>
          <h4>Your California Privacy Rights</h4>
          <p>California Civil Code Section 1798.83, also known as S.B. 27, allows individual customers who have provided their personal information to us to request information about our disclosure of certain categories of personal information to third parties for their direct marketing purposes.<br>      Such requests must be submitted to us by contacting us in accordance with the “Contact Us” section below. Within thirty (30) days of receiving an express written request, we will provide a list of the categories of personal information disclosed to third parties for third party direct marketing purposes during the immediately preceding year, along with the names and addresses of those third parties. This request may be made no more than once per calendar year. We reserve the right not to respond to requests submitted in ways other than those specified above.</p>
          <h4>Governing Law/Arbitration</h4>
          <p>These Terms of Use shall be construed in accordance with and governed by the laws of the United States and the State of California, without reference to their rules regarding conflicts of law. Except for disputes brought in small claims court, all disputes between you and the Company arising out of, relating to, or in connection with the Site or services available on the Site shall be determined by arbitration in the County and City of Los Angeles, California before one arbitrator. The arbitration shall be administered by JAMS pursuant to its Comprehensive Arbitration Rules and Procedures. Judgment on the award may be entered in any court having jurisdiction. If this arbitration provision is found unenforceable or to not apply for a given dispute, then the proceeding must be brought exclusively in a court of competent jurisdiction in the County and City of Los Angeles, California. You hereby accept the exclusive jurisdiction of such court for this purpose.</p>
          <h4>Class Action Waiver</h4>
          <p>Any dispute resolution proceedings, whether in arbitration or court, will be conducted only on an individual basis and not in a class or representative action or as a named or unnamed member in a class, consolidated, representative or private attorney general legal action.<br>      Your access and continued use of the site signifies your explicit consent to this waiver.</p>
          <h4>Contact Us</h4>
          <p>If you have any questions about the Policy, our practices related to the Site, or if you would like to have us remove your information from our database, please contact us at:</p>
          <p>MM Enterprises USA, LLC<br>      Attn: Website Administrator<br>      10115 Jefferson Blvd.<br>      Culver City, California 90232<br>      Email:<a href="mailto:legal@medmen.com"> legal@medmen.com</a></p>
        </div>
        <div class="c-privacy-policy__overlay--420" id="modal-overlay"></div><span class="c-terms-use__close"><a class="close-button" href="#">X</a></span>
        <div class="c-use-terms__modal--420" id="modal-terms">
          <h3>Terms of Use</h3>
          <p class="u-color-red u-font-demibold">Last Updated: March 6, 2018</p>
          <h4>Purpose of Policy</h4>
          <p>
            This is a binding agreement. By using the Internet site located at www.MedMenStores.com (the “Site”) or any services provided in connection with the Site (the “Service”), you agree to abide by these Terms of Use, as they may be amended by MedMen Enterprises LLC, a Nevada limited liability company (the “Company”) from time to time in its sole and absolute discretion. The Company may modify these Terms of Use from time to time in its sole and absolute discretion and without the duty to notify you of such modification. It is your responsibility to review these Terms of Use periodically, and if at any time you find these Terms of Use unacceptable, you must immediately leave the Site and cease all use of the Service and the Site.
            You Agree That By Using The Service You Represent That You Are At Least 21 Years Old And That You Are Legally Able To Enter Into This Agreement.
          </p>
          <h4>California Use Only</h4>
          <p>The Site is controlled and operated by the Company from its offices in the State of California. The Company makes no representation that any of the materials or the Services to which you have been given access are available or appropriate for use in other locations. Your use of or access to the Site should not be construed as Company’s purposefully availing itself of the benefits or privilege of doing business in any state or jurisdiction other than California.</p>
          <h4>Privacy Policy</h4>
          <p>The Company respects your privacy and permits you to control the treatment of your personal information. A complete statement of the Company’s current privacy policy can be found by clicking  clicking here.  The Company’s privacy policy is expressly incorporated into this Agreement by this reference.</p>
          <h4>Account Information</h4>
          <p>When you are required to open an account to use or access the Site or Service, you must complete the registration process by providing the complete and accurate information requested on the registration form. You may also be asked to provide a user name and password.<br>You are entirely responsible for maintaining the confidentiality of your password. You may not use the account, username, or password of someone else at any time. You agree to notify the Company immediately of any unauthorized use of your account, user name, or password.<br>The Company shall not be liable for any loss that you incur as a result of someone else using your password, either with or without your knowledge. You may be held liable for any losses incurred by the Company, its affiliates, officers, directors, employees, consultants, agents, and representatives due to someone else’s use of your account or password.</p>
          <h4>User Content</h4>
          <p>You grant the Company a license to use the materials you post to the Site or Service. By posting, downloading, displaying, performing, transmitting, or otherwise distributing information or other content (“User Content”) to the Site or Service, you are granting the Company, its affiliates, subsidiaries, parents, officers, directors, employees, consultants, agents, and representatives a license to use User Content in connection with the operation of the Site, its affiliates, subsidiaries, parents, officers, directors, employees, consultants, agents, and representatives, including without limitation, a right to copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate, and reformat User Content.<br>You will not be compensated for any User Content. You agree that the Company may publish or otherwise disclose your name in connection with your User Content in its sole and absolute discretion. By posting User Content on the Site or Service, you warrant and represent that you own the rights to the User Content or are otherwise authorized to post, distribute, display, perform, transmit, or otherwise distribute User Content.<br>The Company has the right but not the obligation to monitor and edit or remove any activity or content in its sole and absolute discretion.</p>
          <h4>Compliance with Intellectual Property Laws</h4>
          <p>When accessing the Site or using the Service, you agree to obey by the law and to respect the intellectual property rights of others. Your use of the Service and the Site is at all times governed by and subject to laws regarding copyright ownership and use of intellectual property.<br>You agree not to upload, download, display, perform, transmit, or otherwise distribute any information or content (collectively, “Content”) in violation of any third party’s copyrights, trademarks, or other intellectual property or proprietary rights. You agree to abide by all laws regarding copyright ownership and the use of intellectual property, and you shall be solely responsible for and indemnify the Company against and damages resulting from, any violations of any relevant laws and for any infringements of third party rights caused by any Content you provide or transmit, or that is provided or transmitted using your login information. The burden of proving that any Content does not violate any laws or third party rights rests solely with you.</p>
          <h4>Copyright Infringement</h4>
          <p>The Company has in place certain legally mandated procedures regarding allegations of copyright infringement occurring on the Site or with the Service. The Company has adopted a policy that provides for the immediate suspension and/or termination of any Site or Service user who is found to have infringed on the rights of the Company or of a third party, or otherwise violated any intellectual property laws or regulations.<br>The Company’s policy is to investigate any allegations of copyright infringement brought to its attention. If you have evidence, know, or have a good faith belief that your rights or the rights of a third party have been violated and you want the Company to delete, edit, or disable the material in question, you must provide the Company with all of the following information: (a) a physical or electronic signature of a person authorized to act on behalf of the owner of the exclusive right that is allegedly infringed; (b) identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works are covered by a single notification, a representative list of such works; (c) identification of the material that is claimed to be infringed or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the Company to locate the material; (d) information reasonably sufficient to permit the Company to contact you, such as an address, telephone number, and if available, an electronic mail address at which you may be contacted; (e) a statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (f) a statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. For this notification to be effective, you must provide it to Company’s designated agent at:<br>MedMen Enterprises LLC<br>Attn: Website Administrator<br>8441 Warner Drive<br>Culver City, California 90332<br>Email:<a href="mailto:legal@medmen.com">legal@MedMen.com</a></p>
          <h4>Violations</h4>
          <p>The Company reserves the right to terminate your use of the Service and/or the Site in its sole and absolute discretion. To ensure that the Company provides a high-quality experience for you and for other users of the Site and the Service, you agree that the Company or its representatives may access your account and records on a case-by-case basis to investigate complaints or allegations of abuse, infringement of third party rights, or other unauthorized uses of the Site or the Service. The Company does not intend to disclose the existence or occurrence of such an investigation unless required by law, but the Company reserves the right to terminate your account or your access to the Site immediately, with or without notice to you, and without liability to you, if the Company believes that you have violated any of the Terms of Use, furnished by the Company with false or misleading information, or interfered with use of the Site or the Service by others.</p>
          <h4>No Warranties</h4>
          <p>The Company hereby disclaims all warranties. The Company is making the site available “as is” without warranty of any kind. You assume the risk of any and all damage or loss from use of, or inability to use, the site or the service. To the maximum extent permitted by law, the company expressly disclaims any and all warranties, express or implied, regarding the site, including, but not limited to, any implied warranties of merchantability, fitness for a particular purpose, or noninfringement. The company does not warrant that the site or the service will meet your requirements or that the operation of the site or the service will be uninterrupted or error-free.</p>
          <h4>Limited Liability</h4>
          <p>The Company’s liability to you is limited. To the maximum extent permitted by law, in no event shall the Company be liable for damages of any kind (including, but not limited to, special, incidental, or consequential damages, lost profits, or lost data, regardless of the foreseeability of those damages) arising out of or in connection with your use of the site or any other materials or services provided to you by the company. This limitation shall apply regardless of whether the damages arise out of breach of contract, tort, or any other legal theory or form of action.</p>
          <h4>Affiliated Sites</h4>
          <p>The Company has no control over, and no liability for any third party websites or materials. The Company works with a number of partners and affiliates whose Internet sites may be linked with the Site. Because neither the Company nor the Site has control over the content and performance of these partner and affiliate sites, the Company makes no guarantees about the accuracy, currency, content, or quality of the information provided by such sites, and the Company assumes no responsibility for unintended, objectionable, inaccurate, misleading, or unlawful content that may reside on those sites. Similarly, from time to time in connection with your use of the Site, you may have access to content items (including, but not limited to, websites) that are owned by third parties. You acknowledge and agree that the Company makes no guarantees about, and assumes no responsibility for, the accuracy, currency, content, or quality of this third party content, and that, unless expressly provided otherwise, these Terms of Use shall govern your use of any and all third party content.</p>
          <h4>Prohibited Uses</h4>
          <p>The Company imposes certain restrictions on your permissible use of the Site and the Service. You are prohibited from violating or attempting to violate any security features of the Site or Service, including, without limitation, (a) accessing content or data not intended for you, or logging onto a server or account that you are not authorized to access; (b) attempting to probe, scan, or test the vulnerability of the Service, the Site, or any associated system or network, or to breach security or authentication measures without proper authorization; (c) interfering or attempting to interfere with service to any user, host, or network, including, without limitation, by means of submitting a virus to the Site or Service, overloading, “flooding,” “spamming,” “mail bombing,” or “crashing;” (d) using the Site or Service to send unsolicited e-mail, including, without limitation, promotions, or advertisements for products or services; (e) forging any TCP/IP packet header or any part of the header information in any e-mail or in any posting using the Service; or (f) attempting to modify, reverse-engineer, decompile, disassemble, or otherwise reduce or attempt to reduce to a human-perceivable form any of the source code used by Company in providing the Site or Service. Any violation of system or network security may subject you to civil and/or criminal liability.</p>
          <h4>Indemnity</h4>
          <p>You agree to indemnify and hold harmless the Company for certain of your acts and omissions. You agree to indemnify, defend (with counsel approved by the Company), and hold harmless the Company, its affiliates, officers, directors, employees, consultants, agents, and representatives from any and all third party claims, losses, liability, damages, and/or costs (including reasonable attorney fees and costs) arising from your access to or use of the Site, your violation of these Terms of Use, or your infringement, or infringement by any other user of your account, of any intellectual property or other right of any person or entity. The Company will notify you promptly of any such claim, loss, liability, or demand; provided, that the Company’s failure to notify you shall in no way eliminate or otherwise modify the Company’s rights to indemnification hereunder.</p>
          <h4>Copyright</h4>
          <p>All contents of Site or Service are: Copyright © 2017 MedMen Enterprises LLC, 8441 Warner Drive, Culver City, California. All rights reserved.</p>
          <h4>Trademarks</h4>
          <p>All trademarks, service marks, and trade names of the Company used on the Site or Service are trademarks or registered trademarks of the Company.</p>
          <h4>Governing Law/arbitration</h4>
          <p>These Terms of Use shall be construed in accordance with and governed by the laws of the United States and the State of California, without reference to their rules regarding conflicts of law. Except for disputes brought in small claims court, all disputes between you and the Company arising out of, relating to, or in connection with the Site or Services shall be determined by arbitration in the County and City of Los Angeles, California before one arbitrator. The arbitration shall be administered by JAMS pursuant to its Comprehensive Arbitration Rules and Procedures. Judgment on the award may be entered in any court having jurisdiction. If this arbitration provision is found unenforceable or to not apply for a given dispute, then the proceeding must be brought exclusively in a court of competent jurisdiction in the County and City of Los Angeles, California. You hereby accept the exclusive jurisdiction of such court for this purpose.</p>
          <h4>Class Action Waiver</h4>
          <p>Any dispute resolution proceedings, whether in arbitration or court, will be conducted only on an individual basis and not in a class or representative action or as a named or unnamed member in a class, consolidated, representative or private attorney general legal action.<br>Your access and continued use of the site signifies your explicit consent to this waiver.</p>
          <h4>Severability Waiver</h4>
          <p>If, for whatever reason, a court of competent jurisdiction finds any term or condition in these Terms of Use to be unenforceable, all other terms and conditions will remain unaffected and in full force and effect. No waiver of any breach of any provision of these Terms of Use shall constitute a waiver of any prior, concurrent, or subsequent breach of the same or any other provisions hereof, and no waiver shall be effective unless made in writing and signed by an authorized representative of the waiving party.</p>
          <h4>No License</h4>
          <p>Nothing contained on the Site should be understood as granting you a license to use any of the trademarks, service marks, or logos owned by the Company or by any third party.</p>
          <h4>Modifications</h4>
          <p>The Company may, in its sole and absolute discretion and without notice, (a) revise these Terms of Use; (b) modify the Site and/or the Service; and (c) discontinue the Site and/or Service at any time or from time to time. The Company shall post any revision to these Terms of Use to the Site, and the revision shall be effective immediately on such posting. You agree to review these Terms of Use and other online policies posted on the Site periodically to be aware of any revisions. You agree that, by continuing to use or access the Site following notice of any revision, you shall abide by any such revision.</p>
          <h4>Acknowledgement</h4>
          <p>By using the service or accessing the site, you acknowledge that you have read these terms of use and agree to be bound by them.</p>
        </div>
        <div class="c-use-terms__overlay--420" id="modal-terms-overlay"></div>
        <div class="c-age-gate js-age-gate" title="MedMen - Recreational Dispensary">
          <div class="o-container"><img class="c-age-gate__logo u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/global/agegate/agegate-logo.png" alt="MedMen Logo">
            <div class="c-age-gate__content">
              <div class="js-age-gate__form">
                <h3 class="c-age-gate__title">Are you 21 or older?</h3>
                <label class="c-age-gate__remember"><span class="c-checkbox">
                    <input class="c-checkbox__input js-age-gate__remember" type="checkbox"><span class="c-checkbox__ui"></span></span><span class="c-age-gate__remember__text">Remember me</span></label>
                <div>
                  <ul class="o-list-inline c-age-gate__options">
                    <li class="c-age-gate__option">
                      <button class="c-age-gate__option-btn js-age-gate__yes">Yes</button>
                    </li>
                    <li class="c-age-gate__option c-age-gate__option--divider"></li>
                    <li class="c-age-gate__option">
                      <button class="c-age-gate__option-btn js-age-gate__no">No</button>
                    </li>
                  </ul>
                </div>
              </div>
              <section class="content--420">
                <div class="c-age-gate__content c-age-gate--420">
                  <p>Stay in the loop on our stores, products, and exclusive offers.</p>
                </div>
                <div class="js-newsletter-age">
                  <form class="c-signup c-signup--420 js-newsletter__form">
                    <input class="c-signup__field c-signup__field--420 js-newsletter__email" type="email" name="email" placeholder="Email Address (optional)"><br>
                    <button class="c-signup__submit c-signup__submit--420" role="submit"><span class="u-sr-only">Submit</span>Enter</button>
                    <p class="js-newsletter__error u-text-center" style="display:none;text-transform:uppercase;font-size:80%;margin-top:10px;">Please enter a valid email adddress.</p>
                  </form>
                </div>
                <div class="c-age-gate__content c-age-gate__terms">
                  <p>By clicking Enter, you consent to our storing your email address, and you agree to our<a class="link__terms agegate__terms" href="#">Terms of Service</a> and<a class="link__privacy agegate__terms" href="#">Privacy Policy</a></p>
                </div>
              </section>
              <div class="js-age-gate__not-old-enough" style="display: none">
                <h3 class="c-age-gate__title">Must be 21 years of age to enter this site.</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="c-exit-pop js-exit-pop" title="MedMen - Recreational Dispensary">
          <div class="o-container">
            <div class="c-exit-pop__content">
              <div class="c-exit-pop__close_btn"><a class="close-button" href="#"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/global/close-grey.svg" alt="Close"></a></div>
              <div class="js-exit-pop__form">
                <h1 class="c-exit-pop__title">Get 10% Off<span class="lineTwo">Your First Purchase.</span></h1>
                <p class="c-exit-pop__message">Sign up now to get exclusive offers.</p>
                <form class="c-signup__form js-email__form">
                  <input class="c-signup__field js-email__field" type="email" name="email" placeholder="Email address">
                  <button class="c-signup__submit"><span class="u-sr-only">Submit</span>
                    <svg class="c-signup__submit-icon" xmlns="http://www.w3.org/2000/svg" width="11" height="20" viewbox="0 0 11 20">
                      <path d="M11 10l-9.71 9.993L0 18.66 8.41 10 0 1.339 1.29.007z" fill-rule="evenodd"></path>
                    </svg>
                  </button>
                </form>
                <p class="c-email__error js-email__error">Signup failed, please try again.</p><img class="c-exit-pop__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/global/exitpop/delivery-photo.png" alt="MedMen Logo">
                <p class="c-exit-pop__disclaimer">
                  By signing up, you consent to our storing your email address, receiving periodic email updates, and you agree to our
                  &nbsp;<a class="link__terms" href="#">Terms of Service</a>&nbsp;and&nbsp;<a class="link__privacy" href="#">Privacy Policy.</a>
                </p>
              </div>
              <div class="js-exit-pop__complete cs-exit-pop__complete">
                <h1>Thank you for signing up.</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.9.3/countUp.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <dnn:JQUERY ID="dnnjQuery" runat="server" jQueryHoverIntent="true" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" ></script>
    <script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    <dnn:DnnJsInclude ID="bootstrapJS" runat="server" FilePath="js/bundle.js" PathNameAlias="SkinPath" Priority="15" ForceProvider="DnnFormBottomProvider" />
    <script src="https://kenwheeler.github.io/slick/slick/slick.js" charset="utf-8"></script>