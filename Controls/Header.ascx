﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="MedmenCorporate_2018.Controls.Header" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7J8WCW" height="0" width="0" style="display: none; visibility: hidden"></iframe>
</noscript>
<!-- Global site tag (gtag.js) - Google Analytics-->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-112939841-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-112918227-1');
    gtag('config', 'UA-112939841-1');
</script>

<a class="u-sr-only" id="skip-to-content" href="#content">Skip to main content</a>
<div class="o-wrap animsition">
    <div id="content" role="main">
        <header class="c-header js-mobile-nav js-header-scroll" role="banner" id="BannerHenry">
            <section class="o-container" >
                <div class="c-header__content">
                    <div class="c-header__block">
                        <a class="c-header__logo-link" href="/">
                            <h2 class="u-sr-only">MedMen</h2>
                            <svg class="c-header__logo" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 280 60">
                                <polygon class="c-header__logo-path c-header__logo-path--1" points="28.8,55.3 30,60 31.2,55.3 30,49.7"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--2" points="18.8,49.7 11.1,53.7 19.9,53.5 29.1,49"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--3" points="29.1,48.2 14.4,39.6 0,38 12.1,45.7"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--4" points="29.1,47.3 20.5,28.3 6.9,16.6 13.5,33"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--5" points="35.3,25.5 30,0 24.7,25.5 30,46.9"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--6" points="46.5,33 53.1,16.6 39.5,28.3 30.9,47.3"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--7" points="30.9,48.2 47.9,45.7 60,38 45.6,39.6"></polygon>
                                <polygon class="c-header__logo-path c-header__logo-path--8" points="30.9,49 40.1,53.5 48.9,53.7 41.2,49.7"></polygon>
                                <polygon class="c-header__logo-path--9" points="92.1,21.6 84.6,0 74,0 74,59.1 84.2,59.1 84.2,23.4 92,45.8 92.1,45.8 99.9,23.4 99.9,59.1 110.1,59.1 110.1,0 99.6,0"></polygon>
                                <polygon class="c-header__logo-path--10" points="195.2,21.6 187.7,0 177.2,0 177.2,59.1 187.4,59.1 187.4,23.4 195.2,45.8 195.3,45.8 203.1,23.4 203.1,23.4 203.1,59.1 213.3,59.1 213.3,0 202.7,0"></polygon>
                                <path class="c-header__logo-path--11" d="M278.6 15.9c.1 0 .1-.1.2-.1.1-.1.1-.1.1-.2v-.2c0-.2-.1-.3-.2-.4-.2-.1-.4-.2-.5-.2h-.9v2.1h.3v-1h.5l.6 1h.3l-.7-1c.2.1.2 0 .3 0zm-.6-.1h-.4V15h.7c.1 0 .1 0 .2.1l.1.1v.2c0 .1 0 .1-.1.2 0 .1-.1.1-.1.1h-.2c0 .1-.1.1-.2.1zM267.1 13c-4.1 0-6.5 2.8-8.3 6.3v-5.4h-10.3v45.3h10.3v-32c0-3.3 1.5-5.2 3.6-5.2s3.4 1.9 3.4 5.2v32h10.3V24.8c0-6.9-3.1-11.8-9-11.8zM231.2 13.2c-10.3 0-15.5 10.7-15.5 23.3 0 12.4 4.9 23.5 15.6 23.5 10 0 13.4-10.7 14-15.9h-10.2c-.7 3.6-1.1 7-3.6 7-4 0-4.6-7.4-4.6-11.3h18.8v-3c.1-11.7-3.6-23.6-14.5-23.6zm-4.2 18c.2-3 1-9 4.2-9 3.9 0 3.8 5.3 4.2 9H227zM128.1 13.2c-10.3 0-15.5 10.7-15.5 23.3 0 12.4 4.9 23.5 15.6 23.5 10 0 13.4-10.7 14-15.9H132c-.7 3.6-1.1 7-3.6 7-4 0-4.6-7.4-4.6-11.3h18.8v-3c0-11.7-3.7-23.6-14.5-23.6zm-4.3 18c.2-3 1-9 4.2-9 3.9 0 3.8 5.3 4.2 9h-8.4zM164.2 19.3c-1.6-2.7-3.7-6.3-8.2-6.3-5.6 0-9.9 5.3-9.9 21.5v4.1c0 16.3 4.4 21.5 9.9 21.5 4.5 0 6.7-3.8 8.2-6.5v5.7h10.3V0h-10.3v19.3zm0 15.7v3c0 8.1-1.5 12.4-4.1 12.4s-3.8-4.3-3.8-12.4v-3c0-8 1.2-12.4 3.8-12.4s4.1 4.4 4.1 12.4z"></path>
                                <path class="c-header__logo-path--12" d="M278.8 14.1c-.5-.2-1-.2-1.5 0-.2.1-.4.2-.6.4-.2.2-.3.4-.4.6-.2.5-.2 1 0 1.5.1.2.2.4.4.6.2.2.4.3.6.4.5.2 1 .2 1.5 0 .2-.1.4-.2.6-.4.2-.2.3-.4.4-.6.2-.5.2-1 0-1.5-.1-.4-.5-.8-1-1zm.8 2.4c-.1.2-.2.4-.3.5-.1.1-.3.3-.5.3-.2.1-.4.1-.6.1-.2 0-.4 0-.6-.1-.2-.1-.4-.2-.5-.3-.1-.1-.3-.3-.3-.5-.2-.4-.2-.8 0-1.2.1-.2.2-.4.3-.5.1-.1.3-.3.5-.3.2-.1.4-.1.6-.1.2 0 .4 0 .6.1.4.2.7.5.9.8 0 .3 0 .8-.1 1.2z"></path>
                            </svg></a>
                    </div>
                    <div class="c-header__block">
                        <button class="c-navicon js-mobile-nav__toggle u-hidden@md"><span class="c-navicon__bar c-navicon__bar--1"></span><span class="c-navicon__bar c-navicon__bar--2"></span><span class="c-navicon__bar c-navicon__bar--3"></span><span class="u-sr-only">Toggle Menu</span></button>
                        <nav class="c-header__nav" role="navigation">
                            <ul class="o-list-unstyled c-header__nav-list">
                                <li class="c-header__nav-item"><a class="c-header__nav-link" href="/stores">Stores</a></li>
                                <li class="c-header__nav-item"><a class="c-header__nav-link" href="/what-we-do">What We Do</a></li>
                                <li class="c-header__nav-item"><a class="c-header__nav-link" href="/who-we-are">Who We Are</a></li>
                                <li class="c-header__nav-item"><a class="c-header__nav-link" href="https://investors.medmen.com/home/default.aspx">Investors</a></li>
                                <li class="c-header__nav-item"><a class="c-header__nav-link" href="/blog">Blog</a></li>
                                <li class="c-header__nav-item"><a class="c-header__nav-link" href="/newsroom">Newsroom</a></li>
                                <li class="c-header_nav-item">
                                 <dnn:LOGIN class="topBar" runat="server" ID="dnnLOGIN"/>
                                </li>
                            </ul>

                            <div class="c-header__social">
                                <ul class="o-list-inline c-social u-spacing-none">
                                    <li class="c-social__item"><a class="c-social__item-link" href="https://www.facebook.com/ShopMedMen" target="_blank"><span class="u-sr-only">item.title</span>
                                        <svg class="c-social__item-icon" width="20" height="20" viewbox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z"></path>
                                        </svg></a></li>
                                    <li class="c-social__item"><a class="c-social__item-link" href="https://twitter.com/ShopMedMen" target="_blank"><span class="u-sr-only">item.title</span>
                                        <svg class="c-social__item-icon" width="20" height="20" viewbox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1684 408q-67 98-162 167 1 14 1 42 0 130-38 259.5t-115.5 248.5-184.5 210.5-258 146-323 54.5q-271 0-496-145 35 4 78 4 225 0 401-138-105-2-188-64.5t-114-159.5q33 5 61 5 43 0 85-11-112-23-185.5-111.5t-73.5-205.5v-4q68 38 146 41-66-44-105-115t-39-154q0-88 44-163 121 149 294.5 238.5t371.5 99.5q-8-38-8-74 0-134 94.5-228.5t228.5-94.5q140 0 236 102 109-21 205-78-37 115-142 178 93-10 186-50z"></path>
                                        </svg></a></li>
                                    <li class="c-social__item"><a class="c-social__item-link" href="https://instagram.com/ShopMedMen" target="_blank"><span class="u-sr-only">item.title</span>
                                        <svg class="c-social__item-icon" width="20" height="20" viewbox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1152 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm138 0q0 164-115 279t-279 115-279-115-115-279 115-279 279-115 279 115 115 279zm108-410q0 38-27 65t-65 27-65-27-27-65 27-65 65-27 65 27 27 65zm-502-220q-7 0-76.5-.5t-105.5 0-96.5 3-103 10-71.5 18.5q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103-3 96.5 0 105.5.5 76.5-.5 76.5 0 105.5 3 96.5 10 103 18.5 71.5q20 50 58 88t88 58q29 11 71.5 18.5t103 10 96.5 3 105.5 0 76.5-.5 76.5.5 105.5 0 96.5-3 103-10 71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103 3-96.5 0-105.5-.5-76.5.5-76.5 0-105.5-3-96.5-10-103-18.5-71.5q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10-96.5-3-105.5 0-76.5.5zm768 630q0 229-5 317-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124t-124-322q-5-88-5-317t5-317q10-208 124-322t322-124q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z"></path>
                                        </svg></a></li>
                                </ul>
                            </div>
                        </nav>



                    </div>
                </div>
            </section>
        </header>
