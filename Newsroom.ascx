<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Newsroom.ascx.cs" Inherits="Haven.MedmenCorporate_2018.Newsroom" %>
<%@ Register TagPrefix="Medmen" TagName="Header" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Header.ascx" %>
<%@ Register TagPrefix="Medmen" TagName="Footer" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Footer.ascx" %>

<html class="no-js" lang="en">
<!--#include file = "includes/header/head-meta.ascx" -->
<body>

    <Medmen:Header runat="server" />



    <section class="o-section o-container u-text-center newsroom" id="SectionHenry">
        <%--<section>--%>
        <%-- class="c-page-head"--%>
        <header class="c-page-head" id="HeaderHenry">
            <%--<header style="padding: 3.25vw;">--%>
            <div class="o-container">
                <h1 class="c-page-head__title c-press__header wow fadeInUp" style="visibility: visible; animation-name: fadeInUp; font-weight: 500;">Newsroom.</h1>
            </div>
        </header>
        <div id="ContentPane" runat="server" />
        <%--</section>--%>





        <%--<section>--%>
        <%--   class="c-page-head"--%>

        <div id="DivHenry">
            <div class="o-container">
                <h3 class="c-heading-30 font-demibold u-text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Press Release</h3>
            </div>
            <div class="o-section small-padding">
                <div id="ContentPane2" runat="server" />
            </div>
        </div>
        <%--</section>--%>


        <%--<div class="u-spacing-80"></div>--%>

        <%--<div class="u-spacing-80"></div>--%>


        <%--<section class="o-container o-container--wide" id="touts"></section>
        <div class="o-section small-padding">
            <div class="c-touts">
                <div class="press__release">
                    <article class="c-tout press__release c-touts__item">
                        <div class="c-touts__item--child">
                            <div class="c-tout__presscontent">
                                <h5 class="press__date">08/06/2018</h5>
                                <h3 class="c-heading-19 c-tout__title">Shaping Cannabis Culture; Second Issue of EMBER Hits MedMen Stores</h3>
                                <a class="c-tout__link white" href="/newsroom/2018-08-06-shaping-cannabis-culture-second-issue-of-ember-hits-medmen-stores"><span class="c-more-link c-more-link--highlights">Read More</span><img class="read-more" width="4%" src="/img/global/newsroom/readMoreArrow.svg" alt="back"></a>
                            </div>
                        </div>
                    </article>
                    <article class="c-tout press__release c-touts__item">
                        <div class="c-touts__item--child">
                            <div class="c-tout__presscontent">
                                <h5 class="press__date">07/31/2018</h5>
                                <h3 class="c-heading-19 c-tout__title">Music Executive Jay Brown Joins MedMen�s Board</h3>
                                <a class="c-tout__link white" href="/newsroom/2018-07-31-music-executive-jay-brown-joins-medmens-board"><span class="c-more-link c-more-link--highlights">Read More</span><img class="read-more" width="4%" src="/img/global/newsroom/readMoreArrow.svg" alt="back"></a>
                            </div>
                        </div>
                    </article>
                    <article class="c-tout press__release c-touts__item">
                        <div class="c-touts__item--child">
                            <div class="c-tout__presscontent">
                                <h5 class="press__date">07/31/2018</h5>
                                <h3 class="c-heading-19 c-tout__title">MedMen Receives Approval in New York For Lotions and Topical Sprays</h3>
                                <a class="c-tout__link white" href="/newsroom/2018-07-31-medmen-receives-approval-in-new-york-for-lotions-and-topical-sprays"><span class="c-more-link c-more-link--highlights">Read More</span><img class="read-more" width="4%" src="/img/global/newsroom/readMoreArrow.svg" alt="back"></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>



            <div class="u-spacing-20"></div>
            <a class="c-btn c-btn--outline c-btn--wide c-btn--list-all" href="/press-release-archive/">View All</a>
        </div>--%>
    </section>
    <section class="o-section o-container  small-padding">
        <header class="o-container u-text-center wow fadeInUp">
            <h3 class="u-font-demibold c-heading-30">What's Happening Now</h3>
            <p class="u-spacing-40">Follow us<a class="red" href="https://twitter.com/_AdamBierman_"><img class="twitter-bird" width="4%" src="img/global/newsroom/twitter1 (4).jpg" alt="twitter">@Adam_Bierman</a></p>
        </header>
        <div class="container o-container--wide">
            <div class="col-sm-12" id="example2"></div>
        </div>
    </section>
    <!--#include file = "includes/footer/newsletter.ascx" -->
    <Medmen:Footer runat="server" />
</body>
</html>





