<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home.ascx.cs" Inherits="Haven.MedmenCorporate_2018.Home" %>
<%@ Register TagPrefix="Medmen" TagName="Header" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Header.ascx" %>
<%@ Register TagPrefix="Medmen" TagName="Footer" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Footer.ascx" %>
     
<html class="no-js" lang="en">
    <!--#include file = "includes/header/head-meta.ascx" -->
    <body>
        <Medmen:Header runat="server" />
        <!--#include file = "includes/home/carousel.ascx" -->
        <section class="o-container o-container--wide" id="touts">
            <div class="o-section">
                <div class="c-touts">
                    <div class="c-touts__item">
                        <article class="c-tout"><a class="c-tout__link" href="/stores"><img class="c-tout__thumb u-img-full u-spacing-none" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/touts/homepage-tout-1.jpg" alt="MedMen Stores">
                            <div class="c-tout__content">
                                <h3 class="c-heading-38 c-tout__title">Store Locations</h3><span class="c-btn c-btn--wide">Visit Us</span>
                            </div></a>
                        </article>
                    </div>
                    <div class="c-touts__item">
                    <article class="c-tout"><a class="c-tout__link" href="/what-we-do"><img class="c-tout__thumb u-img-full u-spacing-none" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/touts/homepage-tout-2.jpg" alt="MedMen flowers">
                        <div class="c-tout__content">
                            <h3 class="c-heading-38 c-tout__title">World-Class Facilities</h3><span class="c-btn c-btn--wide">Setting the Bar</span>
                        </div></a>
                    </article>
                    </div>

                    <div class="c-touts__item">
                        <article class="c-tout"><a class="c-tout__link" href="/who-we-are"><img class="c-tout__thumb u-img-full u-spacing-none" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/touts/homepage-tout-3.jpg" alt="MedMen team">
                            <div class="c-tout__content">
                                <h3 class="c-heading-38 c-tout__title">Industry Leaders</h3><span class="c-btn c-btn--wide">On a Mission</span>
                            </div></a>
                        </article>
                    </div>
                </div>
            </div>

            </section>
            <section class="o-section-big c-theme-red" id="quality">
                <div class="o-container">
                    <div class="c-quality">
                        <header class="c-quality__header">
                        <h2 class="c-heading-60 c-quality__title wow fadeInUp">Quality Matters</h2>
                        <div id="ContentPane" class="wow fadeInUp wow-delay-1" runat="server"/>
                        </header>
                        <div class="c-quality__products">
                            <div id="HomePodImageOnePane" runat="server" class="c-quality__product wow rollInCounterClockwise wow-delay-1" />
                            <div id="HomePodImageTwoPane" runat="server" class="c-quality__product wow rollInCounterClockwise wow-delay-2" />
                            <div id="HomePodImageThreePane" runat="server" class="c-quality__product wow rollInCounterClockwise wow-delay-3" />
                        </div>
                    </div>
                </div>
            </section>

        <!--#include file = "includes/home/spectrum.ascx" --> 
        <!--#include file = "includes/footer/instagram.ascx" --> 
        <!--#include file = "/Portals/_default/skins/MedmenCorporate_2018/includes/footer/newsletter.ascx" --> 
        
        <Medmen:Footer runat="server" />
    </body>
</html>











