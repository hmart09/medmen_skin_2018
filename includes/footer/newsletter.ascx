﻿<section class="o-section c-theme-grey-light" id="newsletter">
    <header class="o-container u-text-center wow fadeInUp">
    <h3 class="c-heading-38 u-color-red">Keep in Touch</h3>
    <p class="c-heading-24 u-spacing-40">Stay in the loop on happenings in our stores, new product releases, and exclusive offers.</p>
    </header>
    <div class="wow fadeInUp">
        <div class="js-newsletter">
            <p class="js-newsletter__success u-text-center" style="display: none;">Thank you for signing up!</p>
            <div id="newsletterFormPane" class="c-signup js-newsletter__form" runat="server" />
        </div>
    </div>
</section>