﻿ <section id="instagram"><a href="https://instagram.com/ShopMedMen" target="_blank">
    <ul class="o-list-inline c-gallery">
        <li class="c-gallery__item">
        <div class="js-instagram-carousel"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-1.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-6.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-11.jpg"></div>
        </li>
        <li class="c-gallery__item">
        <div class="js-instagram-carousel"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-2.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-7.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-12.jpg"></div>
        </li>
        <li class="c-gallery__item">
        <div class="js-instagram-carousel"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-3.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-8.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-13.jpg"></div>
        </li>
        <li class="c-gallery__item">
        <div class="js-instagram-carousel"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-4.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-9.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-14.jpg"></div>
        </li>
        <li class="c-gallery__item">
        <div class="js-instagram-carousel"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-5.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-10.jpg"><img class="c-gallery__img u-img-full" data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/instagram/homepage-ig-15.jpg"></div>
        </li>
    </ul></a>
 </section>