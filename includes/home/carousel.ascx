﻿<div class="carousel slide" id="medmenCarousel" data-ride="carousel">
    <div class="carousel-inner">
        <ol class="carousel-indicators">
            <li class="active" data-target="#medmenCarousel" data-slide-to="0"></li>
            <li data-target="#medmenCarousel" data-slide-to="1"></li>
            <li data-target="#medmenCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-item active"><a href="/blog/ember">
            <section class="c-hero c-hero--ember u-text-left" id="hero2">
                <div class="c-hero__backdrop js-hero-parallax"></div>
                <div class="o-container c-hero__container">
                    <div><img class="c-hero--ember-logo" src="/Portals/_default/skins/MedmenCorporate_2018/img/global/home/ember_logo_vector.svg" alt="Ember">
                        <header class="c-heading-ember u-spacing-20 u-font-demibold">A Journal of Cannabis and Culture</header>
                        <p class="c-heading-ember u-spacing-20">Complimentary with Purchase at CA and NY Stores and For Purchase in Select Barnes & Noble Locations.</p>
                        <button class="c-btn c-btn--wide">Learn More</button>
                    </div>
                    <div class="c-hero__promo-small"></div>
                </div>
            </section></a>
        </div>

        <div class="carousel-item"><a href="/stores">
            <section class="c-hero c-hero--bring-friend u-text-left" id="hero3">
                <div class="c-hero__backdrop js-hero-parallax"></div>
                <div class="o-container c-hero__container">
                    <div>
                        <header class="c-hero__promo-header u-spacing-40">Bring a Friend<br /> Get 20% off.</header>
                        <button class="c-btn c-btn--wide">Find Your Store</button>
                    </div>
                    <div class="c-hero__promo-small">
                        <p>Friend must be a first-time customer. Offer cannot be combined with any other coupons, discounts, promotions, or offers and may only be redeemed once per day. Not available in NY stores.</p>
                    </div>
                </div>
            </section></a>
        </div>

        <div class="carousel-item"><a href="/stores/las-vegas-downtown-vegas-arts-district">
            <section class="c-hero c-hero--downtown-vegas" id="hero4">
                <div class="c-hero__backdrop js-hero-parallax">
                <div class="o-container c-hero__container c-hero__container--right">
                    <div class="c-hero__inner u-text-right">
                    <header class="c-heading-38 u-spacing-10 u-font-demibold">Our Newest Store</header>
                    <div class="c-heading-60 u-spacing-40 u-font-demibold">Downtown<span class="u-text-no-break">  Las Vegas</span></div>
                    <div class="c-hero__promo-cta">
                        <button class="c-btn c-btn--wide">Visit Us</button>
                    </div>
                    <p class="c-hero__disclaimer">Keep out of reach of children.<span class="c-hero__disclaimer_line_two">&nbsp;For use only by adults 21 years of age and older.</span></p>
                    </div>
                </div>
                </div>
            </section></a>
        </div>
    </div>
</div>