﻿        <section class="o-section-big c-spectrum" id="the-spectrum">
          <div class="o-container">
            <header class="c-spectrum__header u-text-center wow fadeInUp">
              <h2 class="c-heading-38">The Spectrum</h2>
              <p>We take pride in providing the best products available. We study and test each product before we make it available to you. Not sure what you’re looking for? We can help.</p>
            </header>
            <div class="c-spectrum__content js-strains">
              <div class="js-strains__slider">
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-strain-cbd.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-green">High CBD</h4>
                      <p class="c-spectrum__product-body">Strains that contain more than 2 percent CBD are considered to be high CBD strains. Very high CBD strains may contain over 20 percent. High CBD strains with low THC are believed to provide optimal health benefits and promote body relaxation with minimal psychoactive side effects, while strains with high CBD and high THC are believed to provide a balance of health benefits and cognitive side effects.</p>
                    </header>
                  </div>
                </article>
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-cbd-blend.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-green-light">CBD Blend</h4>
                      <p class="c-spectrum__product-body">This type of strain has been shown to be beneficial because of the entourage effect, where cannabinoids and terpenes help increase the effect of other cannabinoids and/or terpenes.</p>
                    </header>
                  </div>
                </article>
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-strain-sativa.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-yellow">Sativa</h4>
                      <p class="c-spectrum__product-body">Sativa strains are known for eliciting more stimulating, euphoric and uplifting effects than indica strains, making them ideal for daytime use. Characteristics may vary within flowers of the same strain. This variation may be due to the different terpene profile of individual plants.</p>
                    </header>
                  </div>
                </article>
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-strain-sativa-dominant.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-pink">Sativa Dominant</h4>
                      <p class="c-spectrum__product-body">A high THC strain that promotes focus and composure. Perfect for creative endeavors.</p>
                    </header>
                  </div>
                </article>
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-strain-hybrid.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-purple">Hybrid</h4>
                      <p class="c-spectrum__product-body">A hybrid strain is created by cross-breeding two or more cannabis strains. Hybrid may refer to indica and sativa crosses, crosses of two or more sativas, two or more indicas and other variations. Due to their bred genetics, hybrids can offer a desired profile of benefits, often elevating desirable effects while minimizing less-desirable effects depending on personal preference. Hybrids are used to help manage the symptoms of various health conditions.</p>
                    </header>
                  </div>
                </article>
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-strain-indica-dominant.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-blue">Indica Dominant</h4>
                      <p class="c-spectrum__product-body">A high THC strain with a terpene profile that promotes maximum relaxation and a sense of transcendence.</p>
                    </header>
                  </div>
                </article>
                <article class="c-spectrum__product">
                  <div class="c-spectrum__layout">
                    <div class="c-spectrum__product-img">
                      <div class="o-aspect o-aspect--8x6"><img data-flickity-lazyload="/Portals/_default/skins/MedmenCorporate_2018/img/content/strains/homepage-strain-indica.jpg"></div>
                    </div>
                    <header class="c-spectrum__product-header">
                      <h4 class="c-heading-60 c-spectrum__product-title u-color-product-blue-dark">Indica</h4>
                      <p class="c-spectrum__product-body">Indicas are known to evoke a sedative effect, making them popular with evening users. Indicas generally contain a higher level of Myrcene - a terpene thought to produce the “couch-lock” effect, than sativa strains. It has been proposed that the effects of specific strains are dependent on the unique terpene profile of individual plants.</p>
                    </header>
                  </div>
                </article>
              </div>
              <div class="c-spectrum__bar">
                <p class="c-spectrum__disclaimer u-spacing-20@less-than-md u-spacing-40@md">This statement has not been evaluated by the Food and Drug Administration. It is not intended to diagnose, treat, cure, or prevent any disease.</p>
                <div class="c-spectrum__bar-scroll">
                  <ol class="o-list-inline c-spectrum__list">
                    <li class="c-spectrum__list-item">
                      <div  class="c-spectrum__list-btn js-strains__button u-color-product-green"><span class="u-sr-only">High CBD</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 15.196 15.196">
                          <path d="M7.6 15.2a7.6 7.6 0 1 1 7.6-7.6 7.607 7.607 0 0 1-7.6 7.6zM7.6.75a6.848 6.848 0 1 0 6.846 6.85A6.856 6.856 0 0 0 7.6.75z"></path>
                          <path d="M9.25 11.739h-3.3a.375.375 0 0 1-.375-.375V9.625H3.833a.375.375 0 0 1-.375-.375v-3.3a.375.375 0 0 1 .375-.375h1.738V3.833a.375.375 0 0 1 .375-.375h3.3a.375.375 0 0 1 .375.375v1.738h1.739a.375.375 0 0 1 .375.375v3.3a.375.375 0 0 1-.375.375H9.625v1.739a.375.375 0 0 1-.375.379zm-2.929-.75h2.554V9.25a.375.375 0 0 1 .375-.375h1.739V6.321H9.25a.375.375 0 0 1-.375-.375V4.208H6.321v1.738a.375.375 0 0 1-.375.375H4.208v2.554h1.738a.375.375 0 0 1 .375.375z"></path>
                        </svg>
                      </div>
                    </li>
                    <li class="c-spectrum__list-item">
                      <div class="c-spectrum__list-btn js-strains__button u-color-product-green-light"><span class="u-sr-only">CBD Blend</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 9.681 9.681">
                          <path d="M4.842.216a4.626 4.626 0 0 0 0 9.252z"></path>
                          <path d="M4.842 9.681a4.839 4.839 0 1 1 3.424-1.415 4.828 4.828 0 0 1-3.424 1.415zm0-9.247a4.408 4.408 0 1 0 3.118 1.29A4.4 4.4 0 0 0 4.843.435z"></path>
                        </svg>
                      </div>
                    </li>
                    <li class="c-spectrum__list-item">
                      <div class="c-spectrum__list-btn js-strains__button u-color-product-yellow"><span class="u-sr-only">Sativa</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 15.325 15.326">
                          <path d="M7.663 10.91a3.247 3.247 0 1 1 3.247-3.247 3.251 3.251 0 0 1-3.247 3.247zm0-5.744a2.5 2.5 0 1 0 2.5 2.5 2.5 2.5 0 0 0-2.5-2.5zM7.663 2.431a.375.375 0 0 1-.375-.375V.375a.375.375 0 0 1 .75 0v1.681a.375.375 0 0 1-.375.375zM7.663 15.326a.375.375 0 0 1-.375-.375v-1.68a.375.375 0 1 1 .75 0v1.68a.375.375 0 0 1-.375.375zM2.055 8.038H.375a.375.375 0 0 1 0-.75h1.68a.375.375 0 1 1 0 .75zM14.95 8.038h-1.68a.375.375 0 0 1 0-.75h1.68a.375.375 0 0 1 0 .75z"></path>
                          <path d="M3.7 4.073a.373.373 0 0 1-.265-.11L2.244 2.775a.375.375 0 0 1 .53-.53l1.189 1.188a.375.375 0 0 1-.265.64zM12.816 13.191a.373.373 0 0 1-.265-.11l-1.188-1.188a.375.375 0 1 1 .53-.53l1.188 1.188a.375.375 0 0 1-.265.64z"></path>
                          <path d="M2.509 13.191a.375.375 0 0 1-.265-.64l1.188-1.188a.375.375 0 1 1 .53.53l-1.188 1.189a.373.373 0 0 1-.265.109zM11.627 4.073a.375.375 0 0 1-.265-.64l1.188-1.188a.375.375 0 1 1 .53.53l-1.187 1.188a.374.374 0 0 1-.266.11z"></path>
                        </svg>
                      </div>
                    </li>
                    <li class="c-spectrum__list-item">
                      <div class="c-spectrum__list-btn js-strains__button u-color-product-pink"><span class="u-sr-only">Sativa Dominant</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 15.305 15.164">
                          <path d="M5.525 12.043a2.4 2.4 0 1 1 2.4-2.4 2.407 2.407 0 0 1-2.4 2.4zm0-4.06A1.655 1.655 0 1 0 7.18 9.638a1.656 1.656 0 0 0-1.655-1.655zM5.525 6.05a.375.375 0 0 1-.375-.375V4.487a.375.375 0 0 1 .75 0v1.188a.375.375 0 0 1-.375.375zM5.525 15.164a.375.375 0 0 1-.375-.375V13.6a.375.375 0 0 1 .75 0v1.188a.375.375 0 0 1-.375.376zM1.563 10.013H.375a.375.375 0 1 1 0-.75h1.188a.375.375 0 1 1 0 .75zM10.676 10.013H9.489a.375.375 0 1 1 0-.75h1.187a.375.375 0 1 1 0 .75z"></path>
                          <path d="M2.723 7.211a.373.373 0 0 1-.265-.11l-.84-.84a.375.375 0 0 1 .53-.53l.84.84a.375.375 0 0 1-.265.64zM9.167 13.655a.373.373 0 0 1-.265-.11l-.839-.84a.375.375 0 1 1 .53-.53l.839.84a.375.375 0 0 1-.265.64z"></path>
                          <path d="M1.883 13.655a.375.375 0 0 1-.265-.64l.84-.84a.375.375 0 1 1 .53.53l-.84.84a.373.373 0 0 1-.265.11zM8.328 7.211a.375.375 0 0 1-.265-.64l.839-.84a.375.375 0 1 1 .53.53l-.839.84a.373.373 0 0 1-.265.11z"></path>
                          <path d="M11.852 6.567l-1.067-2.161L8.4 4.059l1.727-1.683L9.719 0l2.134 1.122L13.986 0l-.408 2.376L15.3 4.059l-2.386.347zm-1.841-3.032l1.272.185.569 1.153.569-1.153 1.272-.185-.92-.9.217-1.267-1.138.6-1.138-.6.217 1.267z"></path>
                        </svg>
                      </div>
                    </li>
                    <li class="c-spectrum__list-item">
                      <div class="c-spectrum__list-btn js-strains__button u-color-product-purple"><span class="u-sr-only">Hybrid</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 16.151 14.844">
                          <path d="M10.676 7.861a2.385 2.385 0 1 1 2.385-2.385 2.388 2.388 0 0 1-2.385 2.385zm0-4.02a1.635 1.635 0 1 0 1.635 1.635 1.637 1.637 0 0 0-1.635-1.635zM10.676 1.926a.375.375 0 0 1-.375-.375V.375a.375.375 0 0 1 .75 0v1.176a.375.375 0 0 1-.375.375zM10.676 10.951a.375.375 0 0 1-.375-.375V9.4a.375.375 0 0 1 .75 0v1.176a.375.375 0 0 1-.375.375zM6.751 5.851H5.575a.375.375 0 0 1 0-.75h1.176a.375.375 0 0 1 0 .75zM15.776 5.851H14.6a.375.375 0 0 1 0-.75h1.176a.375.375 0 0 1 0 .75z"></path>
                          <path d="M7.9 3.076a.373.373 0 0 1-.265-.11L6.8 2.134a.375.375 0 0 1 .53-.53l.832.832a.375.375 0 0 1-.265.64zM14.283 9.458a.373.373 0 0 1-.265-.11l-.832-.832a.375.375 0 1 1 .53-.53l.832.832a.375.375 0 0 1-.265.64z"></path>
                          <path d="M7.069 9.458a.375.375 0 0 1-.265-.64l.832-.832a.375.375 0 1 1 .53.53l-.832.832a.373.373 0 0 1-.265.11zM13.451 3.076a.375.375 0 0 1-.265-.64l.832-.832a.375.375 0 1 1 .53.53l-.832.832a.373.373 0 0 1-.265.11z"></path>
                          <path d="M5.57 14.844A5.443 5.443 0 0 1 3.4 14.4a5.5 5.5 0 0 1-2.96-2.96 5.551 5.551 0 0 1 .671-5.51 5.474 5.474 0 0 1 2.858-1.992l.957-.285-.532.847a5.065 5.065 0 0 0-.107 5.246A5.047 5.047 0 0 0 6.14 11.6a5.113 5.113 0 0 0 3.378.612l.99-.166-.639.774a5.575 5.575 0 0 1-4.3 2.028zM3.28 5.026a4.773 4.773 0 0 0-1.571 1.356 4.8 4.8 0 0 0-.576 4.761A4.749 4.749 0 0 0 3.7 13.71a4.813 4.813 0 0 0 4.9-.686A5.819 5.819 0 0 1 2.857 7.2a5.69 5.69 0 0 1 .423-2.174z"></path>
                        </svg>
                      </div>
                    </li>
                    <li class="c-spectrum__list-item">
                      <div class="c-spectrum__list-btn js-strains__button u-color-product-blue"><span class="u-sr-only">Indica Dominant</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 13.471 12.52">
                          <path d="M6.121 12.52a5.984 5.984 0 0 1-2.379-.489A6.041 6.041 0 0 1 .488 8.778 5.99 5.99 0 0 1 0 6.4a5.97 5.97 0 0 1 1.221-3.674 6.014 6.014 0 0 1 3.14-2.19L5.318.25l-.532.845a5.64 5.64 0 0 0-.119 5.843 5.652 5.652 0 0 0 5.827 2.746l.989-.165-.638.773a6.127 6.127 0 0 1-4.724 2.228zM3.667 1.616a5.306 5.306 0 0 0-1.848 1.561 5.352 5.352 0 0 0-.642 5.305 5.29 5.29 0 0 0 2.86 2.86 5.361 5.361 0 0 0 5.541-.833h-.01a6.317 6.317 0 0 1-3.214-.859A6.4 6.4 0 0 1 3.16 4.1a6.248 6.248 0 0 1 .507-2.484z"></path>
                          <path d="M10.018 6.568L8.951 4.406l-2.385-.347 1.726-1.683L7.885 0l2.134 1.122L12.153 0l-.409 2.376 1.727 1.684-2.386.347zM8.177 3.536l1.272.185.569 1.153.569-1.153 1.272-.185-.921-.9.217-1.267-1.138.6-1.138-.6.221 1.269z"></path>
                        </svg>
                      </div>
                    </li>
                    <li class="c-spectrum__list-item">
                      <div class="c-spectrum__list-btn js-strains__button u-color-product-blue-dark"><span class="u-sr-only">Indica</span>
                        <svg class="c-spectrum__list-icon" width="32" height="32" viewbox="0 0 13.5 14.441">
                          <path d="M7.21 14.441a7.059 7.059 0 0 1-2.8-.575 7.124 7.124 0 0 1-3.835-3.833A7.048 7.048 0 0 1 0 7.231 7.032 7.032 0 0 1 1.438 2.9a7.094 7.094 0 0 1 3.7-2.58L6.221 0l-.6.958a6.588 6.588 0 0 0-1.038 3.56 6.687 6.687 0 0 0 3.34 5.8 6.737 6.737 0 0 0 4.456.807l1.123-.188-.725.878a7.218 7.218 0 0 1-5.567 2.626zM4.348 1.544a6.281 6.281 0 0 0-2.232 1.871 6.335 6.335 0 0 0-.76 6.285 6.264 6.264 0 0 0 3.388 3.388 6.333 6.333 0 0 0 5.185-.1 6.4 6.4 0 0 0 1.414-.915 7.534 7.534 0 0 1-7.611-7.556 7.368 7.368 0 0 1 .616-2.973z"></path>
                        </svg>
                      </div>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </section>