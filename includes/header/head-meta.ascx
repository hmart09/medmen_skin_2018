﻿
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<dnn:DnnCssInclude runat="server" 
                   FilePath="~/resources/shared/stylesheets/dnndefault/8.0.0/default.css"
                   Priority="13" 
                   Name="dnndefault" 
                   Version="8.0.0" />
<dnn:DnnCssInclude ID="bootStrapCSS" runat="server" FilePath="css/global.css" PathNameAlias="SkinPath" Priority="14" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MedMen Cannabis Dispensaries - Premium Marijuana Dispensary Near Me</title>
    <meta property="og:title" content="MedMen Cannabis Dispensaries - Premium Marijuana Dispensary Near Me">
    <meta property="og:description" content="MedMen marijuana dispensaries provide a wide range of best and high quality cannabis products. Our cannabis dispensary stores in New York, California &amp; Nevada offer a premium shopping experience at low prices. Find the marijuana dispensary near me.">
    <meta name="description" content="MedMen marijuana dispensaries provide a wide range of best and high quality cannabis products. Our cannabis dispensary stores in New York, California &amp; Nevada offer a premium shopping experience at low prices. Find the marijuana dispensary near me.">
    <meta name="keywords" content="marijuana dispensary, dispensary near me, Cannabis dispensaries, California dispensary, New York dispensary, medical marijuana stores, recreational dispensary, Nevada dispensary">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://medmen.com/">
    <meta property="og:image" content="https://medmen.com//Portals/_default/skins/MedmenCorporate_2018/img/meta/1024x1024.jpg">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="MedMen Cannabis Dispensaries - Premium Marijuana Dispensary Near Me">
    <meta name="twitter:description" content="MedMen marijuana dispensaries provide a wide range of best and high quality cannabis products. Our cannabis dispensary stores in New York, California &amp; Nevada offer a premium shopping experience at low prices. Find the marijuana dispensary near me.">
    <meta name="twitter:image" content="https://medmen.com//Portals/_default/skins/MedmenCorporate_2018/img/meta/1024x1024.jpg">
    <meta name="google-site-verification" content="BWj3JQbpVR6rwMXJvB9-bK7S7iORYe5FC01cBfmno-Y">
    <link rel="icon" type="image/png" href="/Portals/_default/skins/MedmenCorporate_2018/img/meta/favicon.png" sizes="16x16">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <style>.async-hide { opacity: 0 !important}</style>
    <script>
        (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
        {'GTM-TL8KZ2F':true});
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-112939841-1', 'auto');
        ga('require', 'GTM-TL8KZ2F');
        ga('send', 'pageview');
    </script>
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N7J8WCW');
    </script>
    <script type="application/ld+json">
        {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "MedMen",
        "description": "MedMen Enterprises is the United States' preeminent cannabis company with multiple assets and operations in California, Nevada and New York. Combined, these key strategic states account for nearly half of North America's addressable legal market. MedMen owns and operates licensed cannabis facilities in cultivation, manufacturing and retail, and is the most recognized cannabis brand in the world today.",
        "url": "https://medmen.com/",
        "telephone": "(855) 292-8399",
        "sameAs": ["https://twitter.com/ShopMedMen","https://www.linkedin.com/company/the-medmen/","https://plus.google.com/+MedMenManagement","https://www.facebook.com/MedMenStores","https://www.youtube.com/channel/UC8M-4QpNJ_58ip7Qpsa3k1g","https://www.instagram.com/ShopMedMen/","https://en.wikipedia.org/wiki/MedMen"]
        }
    </script>
</head>