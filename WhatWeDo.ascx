<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WhatWeDo.ascx.cs" Inherits=" Haven.MedmenCorporate_2018.WhatWeDo" %>
<%@ Register TagPrefix="Medmen" TagName="Header" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Header.ascx" %>
<%@ Register TagPrefix="Medmen" TagName="Footer" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Footer.ascx" %>

<html class="no-js" lang="en">
    <!--#include file = "includes/header/head-meta.ascx" -->
    <body>
        <Medmen:Header runat="server" />
        <section class="c-hero c-hero--whatwedo" id="hero">
          <div class="c-hero__backdrop js-hero-parallax"></div>
          <div class="o-container">
            <h1 class="c-hero__title js-hero-parallax__title wow fadeInUp">What We Do</h1>
          </div>
        </section>
        <section class="o-section-big">
          <div class="o-container">
            <div class="c-stat-counter">
              <div class="c-stat-counter__block">
                <h2 class="c-stat-counter__number u-spacing-none js-ticking-number">19</h2>
                <p class="c-stat-counter__label">Facilities Nationwide</p>
              </div>
              <div class="c-stat-counter__block">
                <div class="c-stat-counter__bridge">located in</div>
              </div>
              <div class="c-stat-counter__block">
                <div class="c-stat-counter__ticker">
                  <div class="c-stat-counter__ticker-inner js-ticker">
                    <h2 class="c-stat-counter__number u-spacing-none js-ticker-item" data-ticker-timer="1">3</h2>
                    <h2 class="c-stat-counter__number u-spacing-none js-ticker-item" data-ticker-timer="1">CA</h2>
                    <h2 class="c-stat-counter__number u-spacing-none js-ticker-item" data-ticker-timer="1">NV</h2>
                    <h2 class="c-stat-counter__number u-spacing-none js-ticker-item" data-ticker-timer="1">NY</h2>
                    <h2 class="c-stat-counter__number u-spacing-none js-ticker-item" data-ticker-timer="-1">3</h2>
                  </div>
                </div>
                <p class="c-stat-counter__label">Key States</p>
              </div>
              <div class="c-stat-counter__block">
                <div class="c-stat-counter__bridge">employing</div>
              </div>
              <div class="c-stat-counter__block">
                <h2 class="c-stat-counter__number u-spacing-none js-ticking-number">800</h2>
                <p class="c-stat-counter__label">and Counting</p>
              </div>
            </div>
          </div>
        </section>
        <section class="c-takeover o-section c-theme-red">
          <div class="o-container">
            <header class="c-takeover__header">
              <h2 class="c-heading-60 u-font-demibold wow fadeInUp">Redefining Cannabis</h2>
              <div id="ContentPane" runat="server" class="u-spacing-none wow fadeInUp wow-delay-1"/>
            </header>
          </div>
          <video class="c-takeover__video" muted autoplay loop>
            <source src="/Portals/_default/skins/MedmenCorporate_2018/video/timeline.webm" type="video/webm">
            <source src="/Portals/_default/skins/MedmenCorporate_2018/video/timeline.mp4" type="video/mp4">
          </video>
        </section>
        <section class="o-section-big c-theme-red u-padding-bottom-80@md">
          <div class="o-container o-container--wide o-container--flush">
            <div class="c-feature-section">
              <div class="c-feature-section__block">
                <div class="c-parallax-figure">
                    <div id="ImageOnePane" runat="server" class="Content Pane o-aspect c-parallax-figure__img js-parallax-figure"/>
                </div>
              </div>
              <div class="c-feature-section__block c-feature-section__block--padded">
                <ul class="o-list-unstyled u-spacing-none c-feature-section__list">
                  <li class="c-feature-section__list-item wow fadeInUp u-spacing-60@sm">
                    <h2 class="c-heading-60 u-font-demibold">Raising the Bar</h2>
                    <div id="RaisingTheBarPane" runat="server" class="ContentPane u-spacing-none wow fadeInUp wow-delay-1"/>
                  </li>
                  <li class="c-feature-section__list-item wow fadeInUp wow-delay-1">
                    <h2 class="c-heading-60 u-font-demibold">Shaping Law</h2>
                    <div id="ShapingLawPane" runat="server" class="ContentPane u-spacing-none wow fadeInUp wow-delay-1"/>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section class="o-section u-padding-top-none@md">
          <div class="o-container o-container--wide o-container--flush">
            <div class="c-feature-section c-feature-section--flip-content">
              <div class="c-feature-section__block c-feature-section__block--padded">
                <div class="c-feature-section__faux-bleed">
                  <div class="wow fadeInUp">
                    <h2 class="c-heading-60 u-font-demibold">Leading Retail</h2>
                    <div id="LeadingRetailPane" runat="server" class="ContentPane u-spacing-none wow fadeInUp wow-delay-1"/>
                  </div>
                </div><a class="c-more-link c-more-link--highlights" href="/stores/">View Our Retail Locations
                  <svg class="c-more-link__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 20">
                    <path d="M24 9.6L14.4 0 13 1.4 20.6 9H0v2h20.6L13 18.6l1.4 1.4 9.6-9.6v-.8z"></path>
                  </svg></a>
              </div>
              <div class="c-feature-section__block">
                <div class="c-parallax-figure">
                  <div id="ImageTwoPane" runat="server" class="o-aspect c-parallax-figure__img js-parallax-figure" />
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--#include file = "/Portals/_default/skins/MedmenCorporate_2018/includes/footer/newsletter.ascx" --> 

       <Medmen:Footer runat="server" />
    </body>
</html>






