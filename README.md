MedmenCorporate_2018
==========

Description
-----------
A DNN Theme built using Christoc's DNN Module and Skin Development Templates, based on the HammerFlex(http://www.christoc.com/Products/HammerFlex) skin/theme for DNN.

Installation and Customization
------------------------------
You install this DNN Skin like any other DNN extension from the Host/Extensions page (do not install via Admin/Extensions), for production use download the INSTALL package from the releases page. For development you can either Fork the project here on GitHub, or you can download the source package. Install the SOURCE package into your development environment. The skin is designed for use in a local development environment at http://local.medmen.com/ 

* Introduction Blog Post on the MedmenCorporate_2018 Skin http://www.chrishammond.com/blog/itemid/2644/introducing-MedmenCorporate_2018-a-new-open-source-skin-for.aspx
* Blog discussing the Layouts available in the MedmenCorporate_2018 Skin http://www.chrishammond.com/blog/itemid/2647/the-layouts-of-the-MedmenCorporate_2018-open-source-skin-for.aspx
* How to use the Carousel in MedmenCorporate_2018 http://www.chrishammond.com/blog/itemid/2645/using-the-bootstrap-carousel-in-the-dnn-MedmenCorporate_2018.aspx
