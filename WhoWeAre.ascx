<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WhoWeAre.ascx.cs" Inherits=" Haven.MedmenCorporate_2018.WhoWeAre" %>
<%@ Register TagPrefix="Medmen" TagName="Header" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Header.ascx" %>
<%@ Register TagPrefix="Medmen" TagName="Footer" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Footer.ascx" %>
<html class="no-js" lang="en">
    <!--#include file = "includes/header/head-meta.ascx" -->
    <body>
         <Medmen:Header runat="server" />
             <section class="c-hero c-hero--who u-spacing-40" id="hero">
          <div class="c-hero__backdrop js-hero-parallax"></div>
          <div class="o-container">
            <h1 class="c-hero__title js-hero-parallax__title wow fadeInUp">Who We Are</h1>
          </div>
        </section>
        <section class="o-section o-container o-container--wide o-container--flush">
          <div class="c-feature-section">
            <div class="c-feature-section__block c-feature-section__block--padded c-feature-section__block--paddedTop">
              <h2 class="c-heading-60 u-font-demibold wow fadeInUp">Our Story</h2>
              <div id="ContentPane" class="ContentPane wow fadeInUp wow-delay-1" runat="server"></div>
            </div>
            <div class="c-feature-section__block">
              <div class="c-parallax-figure">
                <div id="WhoWeAreImageOne" runat="server" class="o-aspect c-parallax-figure__img js-parallax-figure" />
              </div>
            </div>
          </div>
        </section>
        <section class="o-section c-team c-theme-red">
          <div class="c-team__header o-container wow fadeInUp"><span class="c-anchor" id="team"></span>
            <h2 class="u-text-center c-heading-60">The Team</h2>
            <p id="OurTeamPane" class="u-text-centerLipsum u-spacing-80"/>
          </div>
          <div class="c-team__members-group u-spacing-80 js-teamGroup">
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-adam-bierman-thumb.jpg" alt="adam-bierman"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-andrew-modlin-thumb.jpg" alt="andrew-modlin"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-james-parker-thumb.jpg" alt="james-parker"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-chris-ganan-thumb.jpg" alt="chris-ganan"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-lisa-sergi-thumb.jpg" alt="lisa-sergi"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-david-dancer-thumb.jpg" alt="david-dancer"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-morgan-sokol-thumb.jpg" alt="morgon-sokol"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-dyanne-parnel-thumb.jpg" alt="dyanne-parnel"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-barry-fishchetto-thumb.jpg" alt="barry-fishchetto"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-stephanie-van-hassel-thumb.jpg" alt="stephanie-van-hassel"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-sara-connolly-thumb.jpg" alt="sara-connolly"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-vahan-ajamian-thumb.jpg" alt="vahan-ajamian"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-david-chiovetti-thumb.jpg" alt="david-chiovetti"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-ada-lee-thumb.jpg" alt="ada-lee"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-dan-mcclure-thumb.jpg" alt="dan-mcclure"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-erin-shoji-thumb.jpg" alt="erin-shoji"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-dan-edwards-thumb.jpg" alt="edwards"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-daniel-yi-thumb.jpg" alt="daniel-yi"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-kellen-okeefe-thumb.jpg" alt="kellen-okeefe"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-jocelyn-pettway-thumb.jpg" alt="jocelyn-pettway"></div>
            <div class="c-team__member"><img src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-zeeshan-hyder-thumb.jpg" alt="zeeshan-hyder"></div>
          </div>
          <div class="o-container">
            <div class="c-team__member-bio js-teamBio">
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-adam-bierman.jpg" alt="adam-bierman"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Adam Bierman</h4>
                  <h5 class="c-team__position c-heading-19">Co-Founder &amp; CEO</h5>
                  <p>Adam Bierman is the leading voice of the burgeoning, state-sanctioned cannabis industry in the U.S., and represents its new institutional phase of professional standards, evolving regulations and vast commercial potential. Mr. Bierman’s revolutionary views have been featured on news outlets like CNBC, Bloomberg News, Forbes, Time Magazine, the Los Angeles Times, U.S. News & World Report, among others.</p>
                  <p>Mr. Bierman and Andrew Modlin began MedMen nearly a decade ago, first as medical marijuana dispensary operators. The endeavor eventually led to the most dominant cannabis company in the country with operations and hundreds of millions of dollars in assets deployed in the most important markets in North America.</p>
                  <p>He is a champion of sensible, clear and just drug laws that strengthen communities and create business opportunities. MedMen is the single largest financial supporter of progressive marijuana laws at local, state and federal levels.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-andrew-modlin.jpg" alt="andrew-modlin"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Andrew Modlin</h4>
                  <h5 class="c-team__position c-heading-19">Co-Founder &amp; President</h5>
                  <p>Andrew Modlin is the main architect behind MedMen's unique brand and its mainstreaming marijuana message. An artist by training, Mr. Modlin’s vision informs MedMen's industry defining retail concept. He is the recipient of the 2017 "Emerging Leaders Award" by the American Marketing Association. Mr. Modlin is also credited with several innovations in cannabis cultivation and manufacturing, and oversees MedMen’s operational aspects.</p>
                  <p>Mr. Modlin and Adam Bierman began MedMen nearly a decade ago, first as medical marijuana dispensary operators. The endeavor eventually led to the most dominant cannabis company in the country with operations and hundreds of millions of dollars in assets deployed in the most important markets in North America.</p>
                  <p>He is a champion of sensible, clear and just drug laws that strengthen communities and create business opportunities. MedMen is the single largest financial supporter of progressive marijuana laws at local, state and federal levels.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-james-parker.jpg" alt="james-parker"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">James Parker</h4>
                  <h5 class="c-team__position c-heading-19">Chief Financial Officer</h5>
                  <p>James Parker is a seasoned executive with extensive, high level experience in strategic overhaul, business planning and forecasting. He has held senior level positions in consulting, investment and banking. Prior to joining MedMen, Mr. Parker provided interim executive management services to Alvarez & Marsal and was a private equity professional with Leonid Capital. His experience includes operational restructuring and performance optimization across multiple industries, including manufacturing, technology, commercial services, aerospace, and defense.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-chris-ganan.jpg" alt="chris-ganan"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Chris Ganan</h4>
                  <h5 class="c-team__position c-heading-19">Chief Strategy Officer</h5>
                  <p>Chris Ganan brings vast institutional experience in business operations, private equity, investment banking, real estate and FinTech. Mr. Ganan was instrumental in MedMen's capital formation and guides MedMen's investment strategy, ensuring MedMen continues to grow its footprint in North America’s most strategic cannabis markets. Previously, Mr. Ganan was managing member of Cratus Equity, a private investment firm. He has also worked for Alvarez & Marsal, CohnReznick, and Investments Limited.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-lisa-sergi.jpg" alt="lisa-sergi"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">LD Sergi Trager</h4>
                  <h5 class="c-team__position c-heading-19">General Counsel</h5>
                  <p>LD Sergi Trager provides MedMen’s senior management with expert legal advice that informs the company’s business strategy and its implementation. She is directly involved in business transactions and negotiations that are key to MedMen’s success. Ms. Trager was a partner at global consulting firms Deloitte and Rothstein Kass, and practiced law at O’Melveny & Myers, an international law firm. She is a legal expert on alternative investments and holds a Juris Doctorate from UCLA School of Law.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-david-dancer.jpg" alt="david-dancer"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">David Dancer</h4>
                  <h5 class="c-team__position c-heading-19">Chief Marketing Officer</h5>
                  <p>David H. Dancer is charged with creating and implementing MedMen's marketing strategies as the leading brand in the fastest growing industry in the world. For more than 25 years, he has been a leading innovator in retail marketing, branding, e-commerce and customer experience. Prior to joining MedMen, Mr. Dancer was executive vice president and head of marketing for Teleflora, North America’s largest floral service provider, a retail brand consultant for Charles Schwab and marketing vice president for Visa.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-morgan-sokol.jpg" alt="morgan-sokol"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Morgan Sokol</h4>
                  <h5 class="c-team__position c-heading-19">Senior Vice President of Government Affairs</h5>
                  <p>Morgan Sokol manages MedMen's outreach and relationships with government entities, trade organizations and communities in support of the enterprise's mission and the long-term viability of the cannabis industry. Prior to joining MedMen, Ms. Sokol was a grants and quality assurance manager for the Los Angeles Family Housing Corporation, and also worked at the Los Angeles Unified School District, the Los Angeles County Commission on Human Relations, Teach for America and the American Civil Liberties Union.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-dyanne-parnel.jpg" alt="dyanne-parnel"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Dyanne Parnel</h4>
                  <h5 class="c-team__position c-heading-19">Vice President of Quality Systems</h5>
                  <p>Dyanne Parnel oversees MedMen’s quality assurance system that is focused on product safety, consumer satisfaction, biosecurity compliance and promoting continuous improvement. MedMen champions a proactive quality assurance approach. Previously, Ms. Parnel was a corporate QA manager at Ocean Spray. She ensured food and beverage safety at the company’s U.S. and international manufacturing facilities, including agricultural quality, sanitary design standards, supplement creation and auditing and facility chemical sanitation.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-barry-fishchetto.jpg" alt="barry-fishchetto"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Barry Fischetto</h4>
                  <h5 class="c-team__position c-heading-19">Chief Operating Officer</h5>
                  <p>Barry Fischetto oversees MedMen’s retail, cultivation and manufacturing operations, focusing on process reliability and innovation. Prior to joining MedMen, Mr. Fischetto served as senior vice president of operations for coffee grower and distributor Farmer Brothers and as chief operations officer at SK Food Group, a manufacturer and wholesaler of food products to brands like Starbucks, Jamba Juice and Costco.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-stephanie-van-hassel.jpg" alt="stephanie-van-hassel"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Stéphanie Van Hassel</h4>
                  <h5 class="c-team__position c-heading-19">Head of Investor Relations</h5>
                  <p>Stéphanie Van Hassel brings more than a decade of experience in investor relations in the hedge fund world and is the principal liaison between MedMen and the investment community, responsible for communicating the company's growth strategy and performance measures. Prior to joining MedMen, she was co-head of investor services at Atlantic Investment Management, which reached a peak of $2.5 billion in assets during her tenure, and associate vice president at Advent Capital Management.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-sara-connolly.jpg" alt="sara-connolly"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Sara Connolly</h4>
                  <h5 class="c-team__position c-heading-19">Chief of Staff</h5>
                  <p>Sara Connolly translates MedMen’s strategic vision into action, including the development and monitoring of key performance indicators to ensure high performance across the enterprise. Ms. Connolly brings extensive operational expertise from several industries and disciplines to MedMen’s complex operations. She is an expert on data analysis and creative solutions to operational challenges.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-vahan-ajamian.jpg" alt="vahan-ajamian"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Vahan Ajamian</h4>
                  <h5 class="c-team__position c-heading-19">Managing Director of Analyst Relations</h5>
                  <p>Vahan Ajamian is MedMen's principal liaison with brokers and financial analysts covering investment opportunities in the cannabis space. Named a rising star in marijuana investment by Business Insider, he is a capital markets forerunner and expert in the cannabis sector with more than a dozen years of experience in equity research and analysis. Prior to joining MedMen, he was an analyst at Beacon Securities Limited, a full-service independent investment firm in Canada, and at TD Securities.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-david-chiovetti.jpg" alt="david-chiovetti"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">David Chiovetti</h4>
                  <h5 class="c-team__position c-heading-19">Senior Vice President of Retail Operations</h5>
                  <p>David Chiovetti is charged with executing MedMen’s retail strategy, overseeing the company’s rapid expansion in California, Nevada, New York and other emerging markets in North America. He brings more than a decade of success leading similar endeavors in the apparel and fashion industry. He held senior positions at class leading brands like Lucky Brand, True Religion and Guess where he oversaw all aspects of corporate retail including real estate, store design, construction and operations.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-ada-lee.jpg" alt="ada-lee"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Ada Lee</h4>
                  <h5 class="c-team__position c-heading-19">Executive Vice President</h5>
                  <p>Ada Lee oversees MedMen’s internal administration. She manages the company’s corporate office and supports top leadership on special projects with planning and execution. She is charged with creating and implementing procedures that improve and standardize MedMen’s corporate operations. Ms. Lee brings years of experience from the fast-paced, client-driven world of advertising and marketing where she worked with brands like Lexus, Human Rights Campaign and McDonald’s and oversaw the successful launch of several campaigns.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-dan-mcclure.jpg" alt="dan-mcclure"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Dan McClure</h4>
                  <h5 class="c-team__position c-heading-19">Vice President of Agronomy</h5>
                  <p>Dan McClure oversees MedMen's quickly growing cultivation operations. He brings a wealth of institutional knowledge having worked at Driscoll's, one of the longest operating berry producers in the world. He designed clean stock greenhouses, optimized existing production and worked on the company's in vitro germplasm program. Prior to that Mr. McClure was a propagation manager for Fowler Nurseries, a California tree grower for various crops with more than a century of history.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-erin-shoji.jpg" alt="erin-shoji"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Erin Shoji</h4>
                  <h5 class="c-team__position c-heading-19">Vice President of Human Resources</h5>
                  <p>Erin Shoji is instrumental in executing MedMen’s aggressive talent acquisition strategy. Ms. Shoji oversees performance management, benefits, payroll, and ensures the entire enterprise is staffed by motivated and talented professionals who are trained and equipped to execute MedMen’s plan. Before joining MedMen, Ms. Shoji built a successful track record of establishing and leading HR processes and procedures at fast moving startups, including technology innovator Grindr.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-dan-edwards.jpg" alt="dan-edwards"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Dan Edwards</h4>
                  <h5 class="c-team__position c-heading-19">Senior Vice President of Legal Affairs</h5>
                  <p>Dan Edwards manages MedMen’s legal and compliance teams, ensuring operations strictly adhere to applicable laws and regulations. His experience spans a wide array of practice areas including corporate law, mergers & acquisitions, intellectual property, commercial real estate, and cannabis compliance. To date, Mr. Edwards has successfully structured over $100 million of cannabis-related transactions. Prior to joining MedMen, Mr. Edwards worked at Potente APLC, a business law firm in San Diego where he developed and managed the firm's first cannabis division.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-daniel-yi.jpg" alt="daniel-yi"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Daniel Yi</h4>
                  <h5 class="c-team__position c-heading-19">Senior Vice President of Corporate Communications</h5>
                  <p>Daniel Yi is a seasoned communications executive with extensive experience, including management roles at Southern California Edison and the Port of Long Beach. He was also a staff writer and editor at the Los Angeles Times. He is responsible for articulating MedMen's broader narrative and serves as the company’s official spokesperson to the news media.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-kellen-okeefe.jpg" alt="kellen-okeefe"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Kellen O’Keefe</h4>
                  <h5 class="c-team__position c-heading-19">Senior Vice President of Business Development</h5>
                  <p>Kellen O'Keefe brings extensive knowledge of the cannabis industry, strategic partnerships and brand building to lead business development initiatives for MedMen. Prior to joining MedMen, Mr. O'Keefe was a media executive and co-creator of Oakley's groundbreaking experiential marketing program Learn to Ride. He also has built and managed custom brand campaigns for Anheuser-Busch, Chipotle, Crown Imports, T-Mobile, Starbucks and Virgin.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-jocelyn-pettway.jpg" alt="jocelyn-pettway"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Jocelyn Pettway</h4>
                  <h5 class="c-team__position c-heading-19">Director of Design</h5>
                  <p>Jocelyn Pettway heads MedMen’s award winning facilities design and construction management team that includes architects, master draftsmen, interior designers and project managers. The team has executed cultivation, manufacturing and retail projects around the U.S., and MedMen’s 12,000-square- foot headquarters in Los Angeles. Ms. Pettway has a background in theatrical set design and brings her unique perspective to how people interact with space, much like characters on stage.</p>
                </div>
              </div>
              <div class="c-team__member-detail">
                <div class="c-team__portrait"><img class="u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/team/bio-zeeshan-hyder.jpg" alt="zeeshan-hyder"></div>
                <div class="c-team__bio">
                  <h4 class="c-team__name u-font-demibold c-heading-38">Zeeshan Hyder</h4>
                  <h5 class="c-team__position c-heading-19">Senior Vice President of Corporate Development</h5>
                  <p>Zeeshan Hyder brings nearly a decade of experience analyzing and evaluating investment opportunities for large institutions. He was formerly an investment banker at Citigroup where he advised on M&A transactions across the mining, healthcare and consumer industries. Mr. Hyder was also an investment analyst at The Eli and Edythe Broad Foundation's $2 billion investment fund. Most recently Mr. Hyder was a vice president at First Beverage Ventures, a private equity firm focused on emerging food and beverage brands.</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="o-section o-container o-container--wide o-container--flush">
          <div class="c-modal-wrap" data-basiclightbox data-id="1">
            <div class="c-modal">
              <div class="c-stores-app c-stores-app--modal">
                <div class="c-stores-app__layout">
                  <div class="c-stores-app__content">
                    <div class="c-store-details">
                      <div class="c-store-details__close">
                        <button class="c-close"><span class="u-sr-only">Close</span></button>
                      </div>
                      <div class="c-store-details__block">
                        <h2 class="c-heading-38">Jefferson</h2>
                        <address>
                          <div class="c-icon-bullet">
                            <div class="c-icon-bullet__icon">
                              <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 50 50">
                                <circle cx="25" cy="25" r="10.5"></circle>
                                <path d="M25 8.1c9.3 0 16.9 7.6 16.9 16.9S34.3 41.9 25 41.9 8.1 34.3 8.1 25 15.7 8.1 25 8.1M25 5C13.9 5 5 14 5 25s9 20 20 20 20-9 20-20S36.1 5 25 5z"></path>
                              </svg>
                            </div>
                            <ul class="o-list-unstyled">
                              <li>10115 Jefferson Blvd</li>
                              <li>Culver City, CA 90232</li>
                            </ul>
                          </div>
                        </address>
                        <div class="c-icon-bullet">
                          <div class="c-icon-bullet__icon">
                            <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512">
                              <path d="M493.397 24.615l-104-23.997c-11.314-2.611-22.879 3.252-27.456 13.931l-48 111.997a24 24 0 0 0 6.862 28.029l60.617 49.596c-35.973 76.675-98.938 140.508-177.249 177.248l-49.596-60.616a24 24 0 0 0-28.029-6.862l-111.997 48C3.873 366.516-1.994 378.08.618 389.397l23.997 104C27.109 504.204 36.748 512 48 512c256.087 0 464-207.532 464-464 0-11.176-7.714-20.873-18.603-23.385z"></path>
                            </svg>
                          </div>
                          <p>(855) 292-8399</p>
                        </div>
                        <div class="c-icon-bullet">
                          <div class="c-icon-bullet__icon">
                            <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 512 512">
                              <path d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z"></path>
                            </svg>
                          </div>
                          <p><a href="mailto:info@medmen.com">info@medmen.com</a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="c-stores-app__media c-store-details__media">
                    <div class="c-store-details__media-block">
                      <div class="c-cover-img c-cover-img--modal"></div>
                    </div>
                    <div class="c-store-details__media-block">
                      <div class="c-stores-map c-stores-map--modal js-store-map"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="c-feature-section c-feature-section--center">
            <div class="c-feature-section__block">
              <div class="c-parallax-figure">
                <div id="WhoWeAreImageTwoPane" runat="server" class="o-aspect c-parallax-figure__img js-parallax-figure" />
              </div>
            </div>
            <div class="c-feature-section__block c-feature-section__block--padded">
              <h2 class="c-heading-60 u-font-demibold wow fadeInUp">An Iconic Home</h2>
              <div class="wow fadeInUp wow-delay-1">
                <div id="IconicHomePane" class="ContentPane" runat="server"/>
                <div class="c-more-link c-more-link--highlights" data-show-id="1">More Info
                  <svg class="c-more-link__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 20">
                    <path d="M24 9.6L14.4 0 13 1.4 20.6 9H0v2h20.6L13 18.6l1.4 1.4 9.6-9.6v-.8z"></path>
                  </svg>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="c-takeover c-takeover--career o-section c-theme-red">
          <div class="o-container">
            <header class="c-takeover__header">
              <h2 class="c-heading-60 u-font-demibold wow fadeInUp">Careers</h2>
              <div class="wow fadeInUp wow-delay-1">
                <div id="CareersPane" class="u-spacing-none u-spacing-40" runat="server"/><a class="c-more-link u-color-white" href="https://medmen.catsone.com/careers/index.php?m=portal&amp;a=listings&amp;portalID=66363" target="_blank">Apply now
                  <svg class="c-more-link__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 20">
                    <path d="M24 9.6L14.4 0 13 1.4 20.6 9H0v2h20.6L13 18.6l1.4 1.4 9.6-9.6v-.8z"></path>
                  </svg></a>
              </div>
            </header>
          </div>
          <div class="c-takeover__video"></div>
        </section>
        <div class="o-section">
<section class="o-section wow fadeInUp" id="press">
  <div class="o-container">
    <header class="u-text-center">
      <h3 class="c-heading-30">In the Press</h3>
    </header>
    <ul class="o-list-unstyled c-press">
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="Adweek"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-adweek-2x.jpg" alt="Adweek"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="Bloomberg"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-bloomberg-2x.jpg" alt="Bloomberg"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="CNBC"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-cnbc-2x.jpg" alt="CNBC"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="Forbes"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-forbes-2x.jpg" alt="Forbes"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="Los Angeles Times"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-latimes-2x.jpg" alt="Los Angeles Times"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="MSNBC"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-msnbc-2x.jpg" alt="MSNBC"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="Time"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-time-2x.jpg" alt="Time"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="NPR"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-npr-2x.jpg" alt="NPR"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="USA Today"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-usa_today-2x.jpg" alt="USA Today"></a>
      </li>
      <li class="c-press__item">
        <!--a.c-press__link(href=item.link title=item.title)--><a class="c-press__link" href="/newsroom" title="Variety"><img class="c-press__img u-img-respond" src="/Portals/_default/skins/MedmenCorporate_2018/img/content/press/client-logos-variety-2x.jpg" alt="Variety"></a>
      </li>
    </ul>
  </div>
</section>
        </div>
<!--#include file = "/Portals/_default/skins/MedmenCorporate_2018/includes/footer/newsletter.ascx" --> 

        <Medmen:Footer runat="server" />
    </body>
</html>






