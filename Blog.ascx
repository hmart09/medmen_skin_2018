<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Blog.ascx.cs" Inherits="Haven.MedmenCorporate_2018.Blog" %>
<%@ Register TagPrefix="Medmen" TagName="Header" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Header.ascx" %>
<%@ Register TagPrefix="Medmen" TagName="Footer" Src="~/Portals/_default/Skins/MedmenCorporate_2018/Controls/Footer.ascx" %>
<style>
    .full-width {
  width: 100vw;
  position: relative;
  left: 50%;
  right: 50%;
  margin-left: -50vw;
  margin-right: -50vw;
}

</style>     

<html class="no-js" lang="en">


    <!--#include file = "includes/header/head-meta.ascx" -->
    <body>
        <Medmen:Header runat="server" />

        <section class="c-hero c-hero--blog" id="hero">
          <div class="c-hero__backdrop js-hero-parallax"></div>
          <div class="o-container">
            <h1 class="c-hero__title js-hero-parallax__title wow fadeInUp">Blog</h1>
          </div>
        </section>

        <div id="ContentPane" class="ContentPane" runat="server"></div>	

        <%--<section class="o-section o-container">
           <div id="ContentPane" runat="server" />
        </section>--%>



        <%--<div class="contentWrap">
		<div id="topHero" class="topHero" runat="server"></div>
		<div class="skinWidth">
			<div id="ContentPane" class="ContentPane" runat="server"></div>	
			<div class="dnnClear"></div>
			<div id="LeftPane" class="LeftPane" runat="server"></div>
			<div id="RightPane" class="RightPane" runat="server"></div>
			<div class="dnnClear"></div>
			<div id="leftPaneNarrow" class="leftPaneNarrow" runat="server"></div>
			<div id="rightPaneWide" class="rightPaneWide spacingLeft" runat="server"></div>	
			<div class="dnnClear"></div>--%>
			
		</div><!--/skinWidth-->
	</div><!--/contentWrap-->


        <!--#include file = "includes/footer/newsletter.ascx" --> 
        <Medmen:Footer runat="server" />
    </body>
</html>





